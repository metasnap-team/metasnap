#!/bin/sh

set -exu

send_log() {
	if [ ! -e ./log ]; then
		return
	fi
	lastdate=$(grep --only-matching "2[0-9][0-9][0-9][01][0-9][0-3][0-9]T[0-2][0-9][0-5][0-9][0-5][0-9]Z" ./log | tail --lines=1)
	tail --lines=100 ./log | ssh josch@master.debian.org 'mail -v -r "debsnap robot <packages@qa.debian.org>" -s "metasnap cron job FAILED for '"$lastdate"'" -- josch@debian.org' || :
}

[ -e ./updating ] && exit 0

touch ./updating

export PYTHONUNBUFFERED=1

exec 1> ./log 2>&1

twelvehoursago=$(date --utc --date="12 hours ago" +%Y%m%dT%H%M%SZ)

for archive in debian debian-security debian-debug debian-ports debian-backports debian-volatile; do
	ret=0
	./list-timestamps.py $archive || ret=$?
	if [ $ret -ne 0 ]; then
		send_log
		rm ./updating
		touch ./error
		exit 1
	fi

	# remove latest timestamp because timestamps get published even before they are
	# fully mirrored, resulting in 404 errors for Release and Packages files
	# see https://bugs.debian.org/979115
	awk -i inplace '$0 < "'$twelvehoursago'" { print $0; }' by-timestamp/$archive.txt

	ret=0
	/usr/bin/time --verbose ./run.py $archive || ret=$?
	if [ $ret -ne 0 ]; then
		send_log
		rm ./updating
		touch ./error
		exit 1
	fi

	ret=0
	/usr/bin/time --verbose ./run2.py $archive || ret=$?
	if [ $ret -ne 0 ]; then
		send_log
		rm ./updating
		touch ./error
		exit 1
	fi
done

tail --quiet --lines=1 by-timestamp/*.txt | sort | tail --lines=1

if [ -e ./error ] && [ -e ./log ]; then
	tail --lines=100 ./log | ssh josch@master.debian.org 'mail -v -r "debsnap robot <packages@qa.debian.org>" -s "metasnap cron job SUCCEEDED again" -- josch@debian.org'
	rm ./error
fi

rm updating

exit 0
