#!/usr/bin/env python3

import subprocess

for archive in ["debian"]:
    subprocess.check_call(
        ["git", "-C", "by-timestamp/%s.git" % archive, "gc", "--aggressive"]
    )
    conn = sqlite3.connect("by-package/%s.sqlite3" % archive)
    conn.execute("VACUUM")
    conn.close()
