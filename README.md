Dependencies
------------

 - git
 - sqlite3
 - python3
 - python3-lxml
 - python3-pycurl
 - python3-debian
 - time
 - gawk
 - debian-archive-keyring
 - debian-ports-archive-keyring

For the server
--------------

 - nginx
 - python3-certbot-nginx
 - certbot
 - fcgiwrap
 - cron
 - unattended-upgrades

/etc/crontab
------------

    52 */6  * * *   www-data    cd /var/www && ./run.sh

Developer setup
---------------

    mkdir -p by-timestamp by-package
    for a in "" "-security" "-debug" "-ports" "-backports" "-volatile"; do
        ./list-timestamps.py "debian$a"
        git clone "https://metasnap.debian.net/by-timestamp/debian$a.git/.git/" "by-timestamp/debian$a.git"
        curl https://metasnap.debian.net/by-package/debian$a.sqlite3 > "by-package/debian$a.sqlite3"
    done

If you have the necessary privileges you can also set a better git remote URL:

    for a in "" "-security" "-debug" "-ports" "-backports" "-volatile"; do
        git -C "by-timestamp/debian$a.git" remote set-url "origin git@salsa.debian.org:metasnap-team/by-timestamp/debian$a.git"
    done
