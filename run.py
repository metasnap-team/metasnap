#!/usr/bin/env python3

# This script downloads Packages files or their patches from snapshot.d.o and
# stores for each snapshot timestamp, which packages and versions were in each
# suite, component and architecture. The files are committed to a git
# repository because plainly storing those text files for each timestamp would
# require more than half a terabyte of data.

import urllib.request
import re
from lxml import etree
import pycurl
from io import BytesIO
import gzip
import bz2
import lzma
import os
import time
from debian.deb822 import Release, _multivalued
import sys
import hashlib
import subprocess
from collections import defaultdict
import traceback
from pathlib import Path
import json

import quirks

total_downloaded = 0
total_not_downloaded = 0
num_timeouts = 0
num_httpexc = 0
num_timeoutexc = 0
last_request = None


# see https://bugs.debian.org/970824
# and https://salsa.debian.org/python-debian-team/python-debian/-/merge_requests/34
class PdiffIndex(_multivalued):
    _multivalued_fields = {
        "sha1-current": ["SHA1", "size"],
        "sha1-history": ["SHA1", "size", "date"],
        "sha1-patches": ["SHA1", "size", "date"],
        "sha1-download": ["SHA1", "size", "filename"],
        "x-unmerged-sha1-history": ["SHA1", "size", "date"],
        "x-unmerged-sha1-patches": ["SHA1", "size", "date"],
        "x-unmerged-sha1-download": ["SHA1", "size", "filename"],
        "sha256-current": ["SHA256", "size"],
        "sha256-history": ["SHA256", "size", "date"],
        "sha256-patches": ["SHA256", "size", "date"],
        "sha256-download": ["SHA256", "size", "filename"],
        "x-unmerged-sha256-history": ["SHA256", "size", "date"],
        "x-unmerged-sha256-patches": ["SHA256", "size", "date"],
        "x-unmerged-sha256-download": ["SHA256", "size", "filename"],
    }

    @property
    def _fixed_field_lengths(self):
        fixed_field_lengths = {}
        for key in self._multivalued_fields:
            if hasattr(self[key], "keys"):
                # Not multi-line -- don't need to compute the field length for
                # this one
                continue
            length = self._get_size_field_length(key)
            fixed_field_lengths[key] = {"size": length}
        return fixed_field_lengths

    def _get_size_field_length(self, key):
        lengths = [len(str(item["size"])) for item in self[key]]
        return max(lengths)


# see https://salsa.debian.org/python-debian-team/python-debian/-/merge_requests/35
def patches_from_ed_script(source, re_cmd=None):
    i = iter(source)
    if re_cmd is None:
        re_cmd = _patch_re

    for line in i:
        # str/unicode broken for Pattern
        # https://github.com/python/typeshed/issues/1892
        match = re_cmd.match(line)
        if match is None:
            raise ValueError("invalid patch command: %r" % line)

        (first_, last_, cmd) = match.groups()
        first = int(first_)
        last = None if last_ is None else int(last_)

        # using ord() makes this work for str and bytes objects
        if ord(cmd) == 100:  # cmd == d
            first = first - 1
            if last is None:
                last = first + 1
            yield (first, last, [])
            continue

        if ord(cmd) == 97:  # cmd == a
            if last is not None:
                raise ValueError("invalid patch argument: %r" % line)
            last = first
        else:  # cmd == c
            first = first - 1
            if last is None:
                last = first + 1

        lines = []
        for c in i:
            if c in ("", b""):
                raise ValueError("end of stream in command: %r" % line)
            if c in (".\n", ".", b".\n", b"."):
                break
            lines.append(c)
        yield (first, last, lines)


class MyHTTPException(Exception):
    pass


class MyHTTP404Exception(Exception):
    pass


class MyHTTPTimeoutException(Exception):
    pass


def download(url):
    global num_requests
    num_requests += 1
    f = BytesIO()
    maxretries = 10
    for retrynum in range(maxretries):
        try:
            c = pycurl.Curl()
            c.setopt(
                c.URL,
                url,
            )
            c.setopt(pycurl.FOLLOWLOCATION, 1)
            c.setopt(pycurl.MAXREDIRS, 5)
            # even 100 kB/s is too much sometimes
            c.setopt(c.MAX_RECV_SPEED_LARGE, 1000 * 1024)  # bytes per second
            c.setopt(c.CONNECTTIMEOUT, 30)  # the default is 300
            # sometimes, curl stalls forever and even ctrl+c doesn't work
            start = time.time()

            def progress(*data):
                # a download must not last more than 10 minutes
                # with 100 kB/s this means files cannot be larger than 62MB
                if time.time() - start > 10 * 60:
                    print("transfer took too long")
                    # the code will not see this exception but instead get a
                    # pycurl.error
                    raise MyHTTPTimeoutException(url)

            c.setopt(pycurl.NOPROGRESS, 0)
            c.setopt(pycurl.XFERINFOFUNCTION, progress)
            # $ host snapshot.debian.org
            # snapshot.debian.org has address 185.17.185.185
            # snapshot.debian.org has address 193.62.202.27
            # c.setopt(c.RESOLVE, ["snapshot.debian.org:80:185.17.185.185"])
            if f.tell() != 0:
                c.setopt(pycurl.RESUME_FROM, f.tell())
            c.setopt(c.WRITEDATA, f)
            c.perform()
            if c.getinfo(c.RESPONSE_CODE) == 404:
                raise MyHTTP404Exception("got HTTP 404 for %s" % url)
            elif c.getinfo(c.RESPONSE_CODE) not in [200, 206]:
                raise MyHTTPException(
                    "got HTTP %d for %s" % (c.getinfo(c.RESPONSE_CODE), url)
                )
            c.close()
            global total_downloaded
            total_downloaded += len(f.getvalue())
            # if the requests finished too quickly, sleep the remaining time
            # s/r  r/h
            # 3    1020
            # 2.5  1384
            # 2.4  1408
            # 2    1466
            # 1.5  2267
            seconds_per_request = 1.5
            global last_request
            if last_request is not None:
                sleep_time = seconds_per_request - (time.time() - last_request)
                if sleep_time > 0:
                    time.sleep(sleep_time)
            last_request = time.time()
            return f.getvalue()
        except pycurl.error as e:
            code, message = e.args
            if code in [
                pycurl.E_GOT_NOTHING,
                pycurl.E_PARTIAL_FILE,
                pycurl.E_COULDNT_CONNECT,
                pycurl.E_ABORTED_BY_CALLBACK,
                pycurl.E_COULDNT_RESOLVE_HOST,
                pycurl.E_OPERATION_TIMEDOUT,
            ]:
                if retrynum == maxretries - 1:
                    break
                if code == pycurl.E_ABORTED_BY_CALLBACK:
                    # callback was aborted due to timeout
                    global num_timeoutexc
                    num_timeoutexc += 1
                sleep_time = 2 ** (retrynum + 1)
                print("retrying after %f s..." % sleep_time)
                global num_timeouts
                num_timeouts += 1
                time.sleep(sleep_time)
                continue
            else:
                raise
        except MyHTTPException as e:
            print("got HTTP error:", repr(e))
            global num_httpexc
            num_httpexc += 1
            if retrynum == maxretries - 1:
                break
            sleep_time = 4 ** (retrynum + 1)
            print("retrying after %f s..." % sleep_time)
            time.sleep(sleep_time)
            # restart from the beginning or otherwise, the result might
            # include a varnish cache error message
            f = BytesIO()
    raise Exception("failed too often...")


def verify(data, props):
    assert len(data) == props["size"]
    count = 0
    if props.get("md5"):
        assert hashlib.md5(data).hexdigest() == props["md5"]
        count += 1
    if props.get("sha1"):
        assert hashlib.sha1(data).hexdigest() == props["sha1"]
        count += 1
    if props.get("sha256"):
        assert hashlib.sha256(data).hexdigest() == props["sha256"]
        count += 1
    if props.get("sha512"):
        assert hashlib.sha512(data).hexdigest() == props["sha512"]
        count += 1
    assert count > 0


_patch_re = re.compile(rb"^(\d+)(?:,(\d+))?([acd])$")


def get_baseurl(archive, timestamp, suite):
    if archive in ["debian", "debian-backports", "debian-debug", "debian-ports"]:
        return "http://snapshot.debian.org/archive/%s/%s/dists/%s" % (
            archive,
            timestamp,
            suite,
        )
    elif archive == "debian-security":
        if suite in (
            "potato",
            "woody",
            "sarge",
            "etch",
            "lenny",
            "squeeze",
            "wheezy",
            "jessie",
            "jessie-kfreebsd",
            "stretch",
            "buster",
        ):
            # additional /updates/ path-component after suite name
            return "http://snapshot.debian.org/archive/%s/%s/dists/%s/updates" % (
                archive,
                timestamp,
                suite,
            )
        elif suite.endswith("-security"):
            return "http://snapshot.debian.org/archive/%s/%s/dists/%s" % (
                archive,
                timestamp,
                suite,
            )
        else:
            raise Exception("unknown suite: %s" % suite)
    elif archive == "debian-volatile":
        # additional /volatile/ path-component after suite name
        return "http://snapshot.debian.org/archive/%s/%s/dists/%s/volatile" % (
            archive,
            timestamp,
            suite,
        )
    else:
        raise Exception("unknown archive: %s" % archive)


def maybe_patch(archive, timestamp, suite, fname, props, last_props, byhash):
    # FIXME: problem: some diffs only contain lines like:
    # http://snapshot.debian.org/archive/debian/20090121T212634Z/dists/etch-proposBinary files /srv/ftp.debian.org/tiffani/unstable_main_i386.gz and /srv/ftp.debian.org/tiffani/unstable_main_i386.new differ
    # Example: http://snapshot.debian.org/archive/debian/20090122T091438Z/dists/sid/main/binary-i386/Packages.diff/2009-01-22-0236.29.gz
    used_algos = set()
    hash_data = {}
    pdiffindex = None
    try:
        if byhash:
            pdiffidx = download(
                get_baseurl(archive, timestamp, suite)
                + "/%s.diff/by-hash/SHA256/%s"
                % (
                    fname,
                    release_files[fname + ".diff/Index"]["sha256"],
                )
            )
        else:
            pdiffidx = download(
                get_baseurl(archive, timestamp, suite) + "/%s.diff/Index" % fname
            )
        verify(pdiffidx, release_files[fname + ".diff/Index"])
        pdiffindex = PdiffIndex(BytesIO(pdiffidx))
        # FIXME: check for "X-Patch-Precedence: merged"
        # TODO: watch out for illegal field content as it was fixed here:
        # https://salsa.debian.org/ftp-team/dak/-/merge_requests/238
        for algo in ["sha1", "sha256"]:
            if algo + "-current" not in pdiffindex:
                continue
            if algo not in last_props:
                continue
            used_algos.add(algo)
            if algo in release_files[fname]:
                assert (
                    pdiffindex[algo + "-current"][algo.upper()]
                    == release_files[fname][algo]
                )
            assert (
                int(pdiffindex[algo + "-current"]["size"])
                == release_files[fname]["size"]
            )
        # make sure that more than one algo is being used
        assert len(used_algos) > 0
        # make sure that the size is equal independent of the algo
        assert 1 == len(
            set([pdiffindex[algo + "-current"]["size"] for algo in used_algos])
        )
        patches = dict()
        dates = set()
        for algo in used_algos:
            for hist in pdiffindex[algo + "-history"]:
                dates.add(hist["date"])
                if hist["date"] not in patches:
                    patches[hist["date"]] = {
                        "history": {
                            algo: hist[algo.upper()],
                            "size": int(hist["size"]),
                        }
                    }
                    continue
                if "history" in patches[hist["date"]]:
                    assert patches[hist["date"]]["history"]["size"] == int(hist["size"])
                    assert algo not in patches[hist["date"]]["history"], algo
                    patches[hist["date"]]["history"][algo] = hist[algo.upper()]
                else:
                    patches[hist["date"]]["history"] = {
                        "size": int(hist["size"]),
                        algo: hist[algo.upper()],
                    }
            for patch in pdiffindex[algo + "-patches"]:
                dates.add(patch["date"])
                if patch["date"] not in patches:
                    patches[patch["date"]] = {
                        "patches": {
                            algo: patch[algo.upper()],
                            "size": int(patch["size"]),
                        }
                    }
                    continue
                if "patches" in patches[patch["date"]]:
                    assert patches[patch["date"]]["patches"]["size"] == int(
                        patch["size"]
                    )
                    assert algo not in patches[patch["date"]]["patches"]
                    patches[patch["date"]]["patches"][algo] = patch[algo.upper()]
                else:
                    patches[patch["date"]]["patches"] = {
                        "size": int(patch["size"]),
                        algo: patch[algo.upper()],
                    }
            if algo + "-download" not in pdiffindex:
                continue
            for dl in pdiffindex[algo + "-download"]:
                assert dl["filename"].endswith(".gz")
                dl["date"] = dl["filename"][:-3]
                dates.add(dl["date"])
                if dl["date"] not in patches:
                    patches[dl["date"]] = {
                        "download": {
                            algo: dl[algo.upper()],
                            "size": int(dl["size"]),
                        }
                    }
                    continue
                if "download" in patches[dl["date"]]:
                    assert patches[dl["date"]]["download"]["size"] == int(dl["size"])
                    assert algo not in patches[dl["date"]]["download"]
                    patches[dl["date"]]["download"][algo] = dl[algo.upper()]
                else:
                    patches[dl["date"]]["download"] = {
                        "size": int(dl["size"]),
                        algo: dl[algo.upper()],
                    }

        data = last_packages[suite][fname]
        hash_data = {}
        for algo in used_algos:
            hash_data[algo] = last_props[algo]
        hash_data["size"] = last_props["size"]
        assert len(hash_data) > 0
        for date in sorted(dates):
            patch = patches[date]
            if patch["history"]["size"] != len(data):
                continue
            hash_matches = False
            for algo in hash_data:
                if algo not in patch["history"]:
                    continue
                if patch["history"][algo] != hash_data[algo]:
                    hash_matches = False
                    break
                hash_matches = True
            if not hash_matches:
                continue
            diffgz = download(
                get_baseurl(archive, timestamp, suite)
                + "/%s.diff/%s.gz" % (fname, date)
            )
            if "download" in patch:
                assert patch["download"]["size"] == len(diffgz)
                hash_matches = False
                for algo in used_algos:
                    if algo not in patch["download"]:
                        continue
                    if algo == "sha1":
                        assert (
                            hashlib.sha1(diffgz).hexdigest() == patch["download"][algo]
                        )
                    elif algo == "sha256":
                        assert (
                            hashlib.sha256(diffgz).hexdigest()
                            == patch["download"][algo]
                        )
                    else:
                        raise Exception("unknown algo: %s" % algo)
                    hash_matches = True
                assert hash_matches
            diff = gzip.decompress(diffgz)
            assert patch["patches"]["size"] == len(diff)
            hash_matches = False
            for algo in used_algos:
                if algo not in patch["patches"]:
                    continue
                if algo == "sha1":
                    assert hashlib.sha1(diff).hexdigest() == patch["patches"][algo]
                elif algo == "sha256":
                    assert hashlib.sha256(diff).hexdigest() == patch["patches"][algo]
                else:
                    raise Exception("unknown algo: %s" % algo)
                hash_matches = True
            assert hash_matches
            # using patches_from_ed_script is faster than calling the patch
            # utility using subprocess, even considering the time it takes to
            # split and join the lines
            # with open("Packages", "wb") as f:
            #   f.write(data)
            # with open("patch", "wb") as f:
            #   f.write(diff)
            # subprocess.check_call(["patch", "Packages", "patch"])
            # with open("Packages", "rb") as f:
            #   data = f.read()
            # os.unlink("Packages")
            # os.unlink("patch")
            data = data.splitlines(keepends=True)
            diff = diff.splitlines(keepends=True)
            for first, last, args in patches_from_ed_script(diff, re_cmd=_patch_re):
                data[first:last] = args
            data = b"".join(data)
            for algo in used_algos:
                if algo == "sha1":
                    hash_data[algo] = hashlib.sha1(data).hexdigest()
                elif algo == "sha256":
                    hash_data[algo] = hashlib.sha256(data).hexdigest()
                else:
                    raise Exception("unknown algo: %s" % algo)
            hash_data["size"] = len(data)
        hash_matches = False
        for algo in used_algos:
            if algo + "-current" not in pdiffindex:
                continue
            assert hash_data[algo] == pdiffindex[algo + "-current"][algo.upper()], algo
            assert hash_data["size"] == int(pdiffindex[algo + "-current"]["size"])
            hash_matches = True
        assert hash_matches
        hash_matches = False
        for algo in used_algos:
            if algo not in props:
                continue
            assert hash_data[algo] == props[algo]
            hash_matches = True
        assert hash_matches
        assert hash_data["size"] == props["size"]
    except AssertionError as e:
        _, _, tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]
        print("assertion error:", text)
        print("failed to patch -- fallback to downloading a fresh copy")
        return None
    except Exception as e:
        _, _, tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]
        print("caught exception:", repr(e))
        print(text)
        print("failed to patch -- fallback to downloading a fresh copy")
        return None
    else:
        return data


if len(sys.argv) not in (2, 4, 6):
    print("usage: %s archive [min_ts onlysuite [onlycomp onlyarch]]" % sys.argv[0])
    exit(1)
archive = sys.argv[1]
assert archive in [
    "debian",
    "debian-backports",
    "debian-debug",
    "debian-ports",
    "debian-security",
    "debian-volatile",
]
possible_areas = ["main", "contrib", "non-free", "non-free-firmware"]
min_ts = None
onlysuite = None
onlycomp = None
onlyarch = None
if len(sys.argv) in (4, 6):
    min_ts = sys.argv[2]
    onlysuite = sys.argv[3]
if len(sys.argv) == 6:
    onlycomp = sys.argv[4]
    onlyarch = sys.argv[5]

bytimestamp = Path("by-timestamp")
repopath = bytimestamp / ("%s.git" % archive)
timestamps = (bytimestamp / ("%s.txt" % archive)).read_text().splitlines()

if not repopath.exists():
    repopath.mkdir(parents=True)
    subprocess.check_call(["git", "-C", repopath, "init", "--initial-branch=main", "."])
    subprocess.check_call(
        [
            "git",
            "-C",
            repopath,
            "remote",
            "add",
            "origin",
            "git@salsa.debian.org:metasnap-team/by-timestamp/%s.git" % archive,
        ]
    )
else:
    subprocess.check_call(["git", "-C", repopath, "checkout", "main"])

for f in [Path("Release.%s" % archive), Path("Release.%s.gpg" % archive)]:
    if f.exists():
        f.unlink()

if list(repopath.iterdir()) != [repopath / ".git"]:
    # remove files from the working tree and from the index
    subprocess.run(["git", "-C", repopath, "rm", "-r", "."])
    # physically remove all files
    assert repopath / ".git" in repopath.iterdir()
    for path in repopath.iterdir():
        if path.name == ".git":
            continue
        if path.is_symlink():
            # remove symlinks
            path.unlink()
        elif path.is_dir():
            # if it's a directory, check if its content are one of the valid
            # component names and only then remove its content
            # this will fail for unexpected content as a safety measure
            for comp in possible_areas:
                cpath = path / comp
                if not cpath.is_dir():
                    continue
                for e in cpath.iterdir():
                    e.unlink()
                cpath.rmdir()
            path.rmdir()

assert list(repopath.iterdir()) == [repopath / ".git"]

tags = (
    subprocess.check_output(["git", "-C", repopath, "tag"]).decode("utf-8").splitlines()
)

assert tags == timestamps[: len(tags)]
last_release_props = None
last_packages = None
if (
    len(tags) > 0
    and Path("last_cache.%s.%s" % (archive, tags[-1])).exists()
    and Path("last_release_props.%s.%s.json" % (archive, tags[-1])).exists()
):
    last_packages = defaultdict(dict)
    for suite in Path("last_cache.%s.%s" % (archive, tags[-1])).iterdir():
        for area in possible_areas:
            if not (suite / area).is_dir():
                continue
            for arch in (suite / area).iterdir():
                fname = "%s/binary-%s/Packages" % (area, arch.name)
                last_packages[suite.name][fname] = arch.read_bytes()
    last_release_props = json.loads(
        Path("last_release_props.%s.%s.json" % (archive, tags[-1])).read_text()
    )

start_time = time.time()
num_requests = 0
num_handled_timestamps = 0

for i, timestamp in enumerate(timestamps):
    if min_ts is not None and timestamp < min_ts:
        continue
    if timestamp in tags:
        continue
    print(timestamp)
    suites = []
    linktargets = []
    if not quirks.skip_timestamp(archive, timestamp):
        tree = etree.parse(
            BytesIO(
                download(
                    "http://snapshot.debian.org/archive/%s/%s/dists/"
                    % (archive, timestamp)
                )
            ),
            etree.HTMLParser(),
        )
        for e in tree.getroot().xpath(
            "//table[@class='readdir']/tr[td[1]/text()='d']/td[2]/a"
        ):
            suite = e.attrib["href"].rstrip("/")
            if suite == "..":
                continue
            suites.append(suite)
        quirks.validate_suite_dir(archive, timestamp, suites)

        for e in tree.getroot().xpath(
            "//table[@class='readdir']/tr[td[1]/text()='l']/td[2]"
        ):
            dst = e.xpath("a[1]")[0].attrib["href"].rstrip("/")
            src = e.xpath("a[2]")[0].attrib["href"].rstrip("/")
            src = quirks.validate_suite_link(archive, timestamp, suites, dst, src)
            if not src:
                continue

            linktargets.append((src, dst))
            (repopath / dst).symlink_to(src)

    curr_release_props = defaultdict(dict)
    curr_packages = defaultdict(dict)
    for suite in suites:
        if onlysuite is not None and suite != onlysuite:
            continue
        print("suite:", suite)
        if not quirks.validate_suite(archive, timestamp, suite):
            continue
        release_files = dict()

        # download Release and Release.gpg
        rel_data = download(get_baseurl(archive, timestamp, suite) + "/Release")
        assert not os.path.exists("Release.%s" % archive)
        with open("Release.%s" % archive, "wb") as f:
            f.write(rel_data)
        assert not os.path.exists("Release.%s.gpg" % archive)
        rel_gpg = download(get_baseurl(archive, timestamp, suite) + "/Release.gpg")
        with open("Release.%s.gpg" % archive, "wb") as f:
            f.write(rel_gpg)

        # validate Release using Release.gpg with gpgv
        if archive in ["debian", "debian-debug", "debian-security"]:
            keyrings = [
                "--keyring=/usr/share/keyrings/debian-archive-removed-keys.gpg",
                "--keyring=/usr/share/keyrings/debian-archive-keyring.gpg",
            ]
        elif archive == "debian-backports":
            keyrings = [
                "--keyring=/usr/share/keyrings/debian-archive-removed-keys.gpg",
                "--keyring=/usr/share/keyrings/debian-archive-keyring.gpg",
                "--keyring=./backports.gpg",
            ]
        elif archive == "debian-ports":
            keyrings = [
                "--keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg",
                "--keyring=/usr/share/keyrings/debian-ports-archive-keyring-removed.gpg",
            ]
        elif archive == "debian-volatile":
            keyrings = [
                "--keyring=./volatile-woody-sarge.gpg",
                "--keyring=./volatile-etch.gpg",
                "--keyring=./volatile-lenny.gpg",
            ]
        try:
            subprocess.check_call(
                [
                    "gpgv",
                    "--quiet",
                ]
                + keyrings
                + [
                    "Release.%s.gpg" % archive,
                    "Release.%s" % archive,
                ],
                stderr=subprocess.DEVNULL,
            )
            if quirks.skip_verify0(archive, timestamp, suite):
                print("correct signature")
        except subprocess.CalledProcessError:
            if quirks.skip_verify0(archive, timestamp, suite):
                print("wrong signature of Release")
            else:
                raise
        os.unlink("Release.%s" % archive)
        os.unlink("Release.%s.gpg" % archive)
        rel = Release(BytesIO(rel_data))
        byhash = False
        if rel.get("Acquire-By-Hash") == "yes":
            byhash = True
        areas = quirks.validate_components(archive, timestamp, suite, rel["Components"])

        release_arches = rel["Architectures"].split()
        if rel.get("No-Support-for-Architecture-all") == "Packages":
            if "all" in release_arches:
                release_arches.remove("all")
        elif (
            archive == "debian-ports"
            and timestamp >= "20160422T205515Z"
            and suite in ["experimental", "unstable"]
        ):
            # debian-ports publishes Architecture:all without the
            # No-Support-for-Architecture-all field
            # the field got added some time after Nov 2011, which means
            # that even on debian-ports the first branch of this if-statement
            # will get trigged
            release_arches.remove("all")
        else:
            assert "all" not in release_arches
        quirks.validate_release_arches(
            archive, timestamp, suite, release_arches, release_files
        )

        def convert_arch_or_skip(arch):
            if arch in [
                "..",
                "source",
                "debian-installer",
                "by-hash",
                "dep11",
                "i18n",
                "binary-all",
                "upgrade-kernel",
            ]:
                return
            if arch.startswith("installer-"):
                return
            if arch.startswith("disks-"):
                return
            if arch.startswith("Contents-"):
                return
            assert arch.startswith("binary-"), arch
            arch = arch[len("binary-") :]
            assert arch in release_arches, arch
            return arch

        # go through the files referenced by Release and add them with their
        # hashes to release_files[]
        for algo in ["MD5Sum", "SHA1", "SHA256", "SHA512"]:
            for entry in rel.get(algo, []):
                parts = entry["name"].split("/")
                if len(parts) == 3:
                    if archive == "debian-ports" and byhash and parts[0] == "by-hash":
                        # debian-ports ships a by-hash directory in the same
                        # directory as the Release file instead of under
                        # ./${area}/binary-${arch}/by-hash
                        continue
                    area, arch, bname = parts
                elif len(parts) == 4 and parts[3] == "Index":
                    area, arch, bname, _ = parts
                else:
                    continue
                # ignore contrib and non-free for Debian ports as the
                # Packages files were empty for a long time
                if (
                    archive == "debian-ports"
                    and "20220102T032406Z" <= timestamp < "20230219T222402Z"
                    and area in ["contrib", "non-free"]
                ):
                    continue
                assert area in areas, area
                if not quirks.validate_Packages(
                    archive, timestamp, suite, area, arch, bname
                ):
                    continue
                arch = convert_arch_or_skip(arch)
                if arch is None:
                    continue
                if bname not in [
                    "Packages",
                    "Packages.gz",
                    "Packages.bz2",
                    "Packages.xz",
                    "Packages.diff",
                ]:
                    continue
                key = algo.lower()
                if entry["name"] not in release_files:
                    release_files[entry["name"]] = {
                        "size": int(entry["size"]),
                        "area": area,
                        "arch": arch,
                        key: entry[key],
                    }
                else:
                    assert release_files[entry["name"]]["size"] == int(entry["size"]), (
                        key,
                        entry["name"],
                    )
                    assert release_files[entry["name"]]["area"] == area
                    assert release_files[entry["name"]]["arch"] == arch
                    release_files[entry["name"]][key] = entry[key]

        # parse the HTML to check that all Packages files are also listed in
        # the Release file
        for area in areas:
            try:
                data = download(get_baseurl(archive, timestamp, suite) + "/%s/" % area)
            except MyHTTP404Exception:
                if quirks.component_is_404(archive, timestamp, suite, area):
                    continue
                else:
                    raise
            else:
                assert not quirks.component_is_404(archive, timestamp, suite, area)
            tree = etree.parse(BytesIO(data), etree.HTMLParser())
            arches = []
            for e in tree.getroot().xpath(
                "//table[@class='readdir']/tr[td[1]/text()='d']/td[2]/a"
            ):
                arch = e.attrib["href"].rstrip("/")
                if not quirks.validate_Packages_link(
                    archive, timestamp, suite, area, arch, release_arches
                ):
                    continue
                arch = convert_arch_or_skip(arch)
                if arch is None:
                    continue
                arches.append(arch)
                assert "%s/binary-%s/Packages" % (area, arch) in release_files, (
                    area,
                    arch,
                )
            assert sorted(arches) == sorted(release_arches), (
                sorted(arches),
                sorted(release_arches),
            )

        # fill release properties to detect duplicates in the next snapshot timestamp
        for fname, props in release_files.items():
            # skip properties we added manually to make sure that they always
            # get re-downloaded
            if props["size"] == -1:
                continue
            curr_release_props[suite][fname] = props

        # create subdirectories unconditionally so that even suites without any
        # Packages files have a directory structure and thus do not cause
        # broken symlinks to them (example: oldstable-updates -> jessie-updates)
        for area in areas:
            (repopath / suite / area).mkdir(parents=True, exist_ok=True)

        # download all Packages files and add the package versions to git
        for fname, props in release_files.items():
            if fname.endswith("/Packages.gz") or fname.endswith("/Packages.xz"):
                assert fname[:-3] in release_files
                continue
            elif fname.endswith("/Packages.bz2"):
                assert fname[:-4] in release_files
                continue
            elif fname.endswith("/Packages.diff/Index"):
                assert fname[:-11] in release_files
                continue
            assert fname.endswith("/Packages")
            arch = props["arch"]
            area = props["area"]
            if onlyarch is not None and onlyarch != arch:
                continue
            if onlycomp is not None and onlycomp != area:
                continue
            # no need to download anything if Packages file is of zero size
            if props["size"] == 0:
                (repopath / suite / area / arch).touch()
                print(archive, timestamp, suite, fname, "(zero size)")
                curr_packages[suite][fname] = b""
                continue
            # restore the last file if the Packages hashes are the same as the last
            #
            # There is a downside: sometimes the hashes in the Release file are
            # the same as the last but the underlying Packages file changed and
            # doesn't match the hash.
            if (
                last_release_props is not None
                and suite in last_release_props
                and fname in last_release_props[suite]
                and props == last_release_props[suite][fname]
            ):
                not_downloaded = release_files.get(
                    fname + ".xz",
                    release_files.get(fname + ".bz2", release_files.get(fname + ".gz")),
                )
                assert not_downloaded is not None
                total_not_downloaded += not_downloaded["size"]
                subprocess.check_call(
                    [
                        "git",
                        "-C",
                        repopath,
                        "reset",
                        "--quiet",
                        "HEAD",
                        "%s/%s/%s" % (suite, area, arch),
                    ]
                )
                subprocess.check_call(
                    [
                        "git",
                        "-C",
                        repopath,
                        "checkout",
                        "--quiet",
                        "--",
                        "%s/%s/%s" % (suite, area, arch),
                    ]
                )
                print(archive, timestamp, suite, fname, "(no changes)")
                curr_packages[suite][fname] = last_packages[suite][fname]
                continue
            skip_verify1, skip_verify2 = quirks.skip_verify12(
                archive, timestamp, suite, area, arch
            )
            data = None
            if (
                last_packages is not None
                and suite in last_packages
                and fname in last_packages[suite]
                and fname + ".diff/Index" in release_files
            ):
                # apply diff to last downloaded file
                print(archive, timestamp, suite, fname, "(patching...)")
                data = maybe_patch(
                    archive,
                    timestamp,
                    suite,
                    fname,
                    props,
                    last_release_props[suite][fname],
                    byhash,
                )
                if data is not None:
                    not_downloaded = release_files.get(
                        fname + ".xz",
                        release_files.get(
                            fname + ".bz2", release_files.get(fname + ".gz")
                        ),
                    )
                    assert not_downloaded is not None
                    total_not_downloaded += not_downloaded["size"]
            if data is None:
                # download Packages file anew
                print(archive, timestamp, suite, fname, "(downloading...)")
                assert (
                    fname + ".gz" in release_files
                    or fname + ".xz" in release_files
                    or fname + ".bz2" in release_files
                )
                if fname + ".xz" in release_files:
                    fname_compressed = fname + ".xz"
                elif fname + ".bz2" in release_files:
                    fname_compressed = fname + ".bz2"
                else:
                    fname_compressed = fname + ".gz"
                # use the by-hash link if the hash in the Release file is correct
                if byhash and not skip_verify1:
                    data = download(
                        get_baseurl(archive, timestamp, suite)
                        + "/%s/binary-%s/by-hash/SHA256/%s"
                        % (
                            area,
                            arch,
                            release_files[fname_compressed]["sha256"],
                        )
                    )
                else:
                    data = download(
                        get_baseurl(archive, timestamp, suite) + "/" + fname_compressed
                    )
                try:
                    verify(data, release_files[fname_compressed])
                    if skip_verify1:
                        print("correct checksums1")
                except AssertionError:
                    if skip_verify1:
                        print("wrong checksums1:", fname_compressed)
                    else:
                        raise
                if fname_compressed.endswith(".gz"):
                    data = gzip.decompress(data)
                elif fname_compressed.endswith(".bz2"):
                    data = bz2.decompress(data)
                else:
                    data = lzma.decompress(data)
            try:
                verify(data, props)
                if skip_verify2:
                    print("correct checksums2")
            except AssertionError:
                if skip_verify2:
                    print("wrong checksums2:", fname)
                else:
                    raise
            curr_packages[suite][fname] = data
            packages = []
            ## using Deb822 is 12 times slower
            ## even with use_apt_pkg=True it's 4 times slower than manual parsing
            # for pkg in Deb822.iter_paragraphs(data, use_apt_pkg=True):
            #    packages.append((pkg["Package"], pkg["Version"]))
            pkgname = None
            pkgver = None
            data = quirks.fixup_data(archive, timestamp, suite, area, arch, data)
            for line in data.splitlines():
                if line == b"":
                    assert pkgname is not None
                    assert pkgver is not None
                    packages.append((pkgname, pkgver))
                    pkgname = None
                    pkgver = None
                elif line.startswith(b"Package: "):
                    pkgname = line[9:].lstrip().decode("ascii")
                elif line.startswith(b"Version: "):
                    pkgver = line[9:].lstrip().decode("ascii")
            assert pkgname is None
            assert pkgver is None
            # Sort the list of packages to minimize the diff between timestamps
            # in the git repo. Sometimes just the order of packages changes and
            # we are not interested in the order.
            (repopath / suite / area / arch).write_text(
                "".join("%s %s\n" % (pkg, ver) for pkg, ver in sorted(packages))
            )

    assert curr_release_props.keys() == curr_packages.keys()
    last_release_props = curr_release_props
    last_packages = curr_packages

    # sometimes, broken symlinks are still created in cases where a directory
    # tree is empty because empty directories are not stored by git
    if onlysuite is None:
        for src, dst in linktargets:
            if not quirks.validate_suite(archive, timestamp, src):
                print("removing dead symlink", dst, "to", src)
                (repopath / dst).unlink()
                continue
            assert (repopath / src).exists(), src

    match = re.fullmatch(r"(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)Z", timestamp)
    assert match is not None
    iso8601 = "%s-%s-%sT%s:%s:%sZ" % match.groups()
    # make sure that there is no unexpected file in the root of the repo
    for path in repopath.iterdir():
        assert not path.is_file(), path
    subprocess.check_call(["git", "-C", repopath, "add", "."])
    subprocess.check_call(
        [
            "git",
            "-C",
            repopath,
            "commit",
            "--allow-empty",  # especially in the old snapshots, sometimes, nothing changed
            "--no-gpg-sign",
            "--date=%s" % iso8601,
            "--author=debsnap robot <packages@qa.debian.org>",
            "--message=%s" % timestamp,
        ],
        env={
            "GIT_COMMITTER_DATE": iso8601,
            "GIT_COMMITTER_NAME": "debsnap robot",
            "GIT_COMMITTER_EMAIL": "packages@qa.debian.org",
        },
    )
    subprocess.check_call(["git", "-C", repopath, "tag", "--no-sign", timestamp])
    if not quirks.skip_timestamp(archive, timestamp):
        subprocess.check_call(["git", "-C", repopath, "rm", "-r", "."])
    print("total downloaded: %d bytes" % total_downloaded)
    print("total not downloaded: %d bytes" % total_not_downloaded)
    print("total num timeouts: %d" % num_timeouts)
    print("total num http exc: %d" % num_httpexc)
    print("total num timeout exc: %d" % num_timeoutexc)
    print("total num requests: %d" % num_requests)
    print(
        "current requests per hour: ",
        num_requests * 60 * 60 / (time.time() - start_time),
    )

    num_handled_timestamps += 1
    total_hours = (time.time() - start_time) / (60 * 60)
    ts_per_h = num_handled_timestamps / total_hours
    remaining = (len(timestamps) - i - 1) / ts_per_h
    print("timestamps per hour: %f remaining: %f days" % (ts_per_h, remaining / 24))

if num_handled_timestamps == 0:
    print("nothing to be done")
    exit(0)

# clear last cache(s)
# there might be more than one in case some runs have failed and thus never
# cleaned up after themselves
for p in Path(".").glob("last_cache.%s.*" % archive):
    for suite in p.iterdir():
        for area in possible_areas:
            if not (suite / area).is_dir():
                continue
            for arch in (suite / area).iterdir():
                arch.unlink()
            (suite / area).rmdir()
        suite.rmdir()
    p.rmdir()
for p in Path(".").glob("last_release_props.%s.*.json" % archive):
    p.unlink()

# write out last_packages to the cache
for suite in last_packages:
    for fname in last_packages[suite]:
        area, arch, bname = fname.split("/")
        assert area in possible_areas
        (Path("last_cache.%s.%s" % (archive, timestamp)) / suite / area).mkdir(
            parents=True, exist_ok=True
        )
        assert arch.startswith("binary-")
        arch = arch[len("binary-") :]
        assert bname == "Packages"
        (
            Path("last_cache.%s.%s" % (archive, timestamp)) / suite / area / arch
        ).write_bytes(last_packages[suite][fname])
# write out last_release_props
Path("last_release_props.%s.%s.json" % (archive, timestamp)).write_text(
    json.dumps(last_release_props)
)

subprocess.check_call(["git", "-C", repopath, "reset", "--hard", "main"])
subprocess.check_call(["git", "-C", repopath, "update-server-info"])
if (
    subprocess.check_output(
        ["git", "-C", repopath, "remote", "get-url", "origin"]
    ).decode("utf-8")
    == "git@salsa.debian.org:metasnap-team/by-timestamp/%s.git\n" % archive
):
    subprocess.check_call(["git", "-C", repopath, "push", "origin", "main"])
