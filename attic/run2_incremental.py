#!/usr/bin/env python3

import sys
import subprocess
import os
from collections import defaultdict
from debian.debian_support import AptPkgVersion

if len(sys.argv) not in (3, 6):
    print("usage: %s archive timestamp [onlysuite onlyarea onlyarch]" % sys.argv[0])
    exit(1)
archive = sys.argv[1]
timestamp = sys.argv[2]
onlysuite = None
onlyarea = None
onlyarch = None
if len(sys.argv) == 6:
    onlysuite, onlyarea, onlyarch = sys.argv[3:6]

timestamps = (
    subprocess.check_output(["git", "-C", "./by-timestamp/%s.git" % archive, "tag"])
    .decode("utf-8")
    .splitlines()
)
i = timestamps.index(timestamp)
subprocess.check_call(
    [
        "git",
        "-C",
        "./by-timestamp/%s.git" % archive,
        "checkout",
        "--quiet",
        timestamp,
    ]
)

last_timestamp = None
if i != 0:
    last_timestamp = timestamps[i - 1]

# make sure that no timestamp is skipped by comparing the timestamp we provided
# to the last line of timestamps.txt
if last_timestamp is not None and os.path.exists("by-suite/timestamps.txt"):
    with open("by-suite/timestamps.txt") as f:
        current_timestamp = None
        for line in f:
            current_timestamp = line.rstrip()
    assert current_timestamp == last_timestamp

timestamp_dt = datetime.strptime(timestamp, "%Y%m%dT%H%M%S%z").timestamp()

for suite in os.listdir("./by-timestamp/%s.git" % archive):
    if suite == ".git":
        continue
    if onlysuite is not None and onlysuite != suite:
        continue
    suite_changed = False
    for area in ["main", "contrib", "non-free"]:
        if onlyarea is not None and onlyarea != area:
            continue
        area_changed = False
        for arch in os.listdir("./by-timestamp/%s.git/%s/%s" % (archive, suite, area)):
            if onlyarch is not None and onlyarch != arch:
                continue
            os.makedirs(
                "by-suite/%s/%s/%s/%s" % (archive, suite, area, arch),
                exist_ok=True,
            )
            new_versions = dict()
            with open(
                "./by-timestamp/%s.git/%s/%s/%s" % (archive, suite, area, arch),
                encoding="ascii",
            ) as f:
                for line in f:
                    pkg, ver = line.rstrip().split(" ", maxsplit=1)
                    if pkg in new_versions:
                        new_versions[pkg].add(ver)
                    else:
                        new_versions[pkg] = set([ver])

            for pkg in new_versions:
                fname = "by-suite/%s/%s/%s/%s/%s" % (archive, suite, area, arch, pkg)
                if not os.path.exists(fname):
                    # write out new file
                    with open(fname, "w") as f:
                        for ver in sorted(new_versions[pkg], key=AptPkgVersion):
                            f.write("%s %s %s\n" % (ver, timestamp, timestamp))
                    os.utime(fname, times=(timestamp_dt))
                    continue
                # read in file and extend each entry that ends with the last
                # timestamp and matches this version
                lines = list()
                with open(fname) as f:
                    for line in f:
                        ver, first_ts, last_ts = line.rstrip().split(" ", maxsplit=2)
                        if ver not in new_versions[pkg]:
                            # the version is not part of the current timestamp and
                            # thus doesn't concern us
                            lines.append("%s %s %s\n" % (ver, first_ts, last_ts))
                            continue
                        # the version was found in the current timestamp: check
                        # if last_ts is the timestamp before the current one
                        if last_ts == last_timestamp:
                            # extend the range
                            lines.append("%s %s %s\n" % (ver, first_ts, timestamp))
                            new_versions[pkg].remove(ver)
                            continue
                        # the version was in the current timestamp but didn't
                        # end with the last timestamp, thus there is a gap and
                        # we have to start a new range
                        lines.append("%s %s %s\n" % (ver, timestamp, timestamp))
                        new_versions[pkg].remove(ver)
                # everything that is still in new_versions and was not deleted
                # is a version that wasn't seen before
                for ver in sorted(new_versions[pkg], key=AptPkgVersion):
                    lines.append("%s %s %s\n" % (ver, timestamp, timestamp))
                with open(fname, "w") as f:
                    for line in lines:
                        f.write(line)
                os.utime(fname, times=(timestamp_dt))

            # update directory timestamp if something changed
            if new_versions:
                area_changed = True
                os.utime(
                    "by-suite/%s/%s/%s/%s" % (archive, suite, area, arch),
                    times=(timestamp_dt),
                )
        if area_changed:
            os.utime("by-suite/%s/%s/%s" % (archive, suite, area), times=(timestamp_dt))
    if suite_changed:
        os.utime("by-suite/%s/%s" % (archive, suite), times=(timestamp_dt))

with open("by-suite/timestamps.txt", "a") as f:
    f.write(timestamp + "\n")
