#!/usr/bin/env python3

from debian.deb822 import Deb822
from operator import itemgetter
import re
import sys
from pathlib import Path

dep_re = re.compile(
    r"""
        ^\                                # starts with a space
        (?P<name>[a-z0-9][a-z0-9+-.]+)    # package name
        (:(?P<arch>[a-z0-9-]+)\w)?        # optional arch qualifier
        \                                 # separated by a space
        \(=\ (?P<ver>[A-Za-z0-9.+-~:]+)\) # version
        ,?$                               # optional comma at the end
        """,
    re.VERBOSE,
)

with open(sys.argv[1]) as f:
    buildinfo = Deb822(f)

ranges = []
for line in buildinfo["Installed-Build-Depends"].splitlines():
    if line == "":
        continue
    m = dep_re.fullmatch(line)
    if m is None:
        raise Exception("regex doesn't match on %s" % line)
    name = m.group("name")
    arch = m.group("arch") or buildinfo["Build-Architecture"]
    ver = m.group("ver")
    r = []
    # for comp in ["main", "contrib", "non-free"]:
    for comp in ["main"]:
        fname = Path("by-suite/debian/unstable/") / comp / arch / name
        if not fname.exists():
            raise Exception("cannot find package for %s" % str((line, name, arch, ver)))
        with open(fname, "r") as f:
            for l in f:
                l = l.strip()
                v, s, e = l.split()
                if v != ver:
                    continue
                r.append((s, e))
    if len(r) == 0:
        raise Exception("version not found for %s" % line)
    if len(r) > 1:
        raise Exception("multiple ranges found for %s" % line)
    ranges.append(r[0])

# This algorithm is similar to Interval Scheduling
# https://en.wikipedia.org/wiki/Interval_scheduling
# But instead of returning the ranges, we return the endtime of ranges
# See also:
# https://stackoverflow.com/questions/27753830/
# https://stackoverflow.com/questions/4962015/
# https://cs.stackexchange.com/questions/66376/
# https://stackoverflow.com/questions/52137509/
# https://www.codechef.com/problems/ONEKING
# https://discuss.codechef.com/t/oneking-editorial/9096
ranges.sort(key=itemgetter(1))
result = list()
last = "19700101T000000Z"  # impossibly early date
for b, e in ranges:
    if last >= b:
        continue
    last = e
    result.append(last)
print(result)
