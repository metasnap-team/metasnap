#!/usr/bin/env python3

import os
from pathlib import Path
from datetime import datetime
from collections import defaultdict
import subprocess
import sys

archive = sys.argv[1]

packages = {
    "main": defaultdict(lambda: defaultdict(set)),
    "contrib": defaultdict(lambda: defaultdict(set)),
    "non-free": defaultdict(lambda: defaultdict(set)),
}

for suite in (Path("by-suite") / archive).iterdir():
    for comp in suite.iterdir():
        assert comp.name in ["main", "contrib", "non-free"]
        for arch in comp.iterdir():
            print(arch)
            for pkg in arch.iterdir():
                assert pkg.is_file()
                packages[comp.name][arch.name][pkg.name].add(suite.name)

num_total = 0
for comp in ["main", "contrib", "non-free"]:
    for arch in packages[comp]:
        num = len(packages[comp][arch])
        num_total += num
        print("%s %s: %d" % (comp, arch, num))
print("total:", num_total)

i = 0
for comp in ["main", "contrib", "non-free"]:
    for arch in packages[comp]:
        for pkg in packages[comp][arch]:
            print("progress: %f %%\r" % (100 * i / num_total), end="")
            versions = defaultdict(lambda: defaultdict(set))
            for suite in packages[comp][arch][pkg]:
                with open(Path("by-suite") / archive / suite / comp / arch / pkg) as f:
                    for line in f:
                        ver, rest = line.rstrip().split(" ", maxsplit=1)
                        versions[ver][suite].add(rest)
            os.makedirs(
                "by-package/%s/%s/%s/%s" % (archive, comp, arch, pkg), exist_ok=True
            )
            for ver in versions:
                with open(
                    "by-package/%s/%s/%s/%s/%s" % (archive, comp, arch, pkg, ver), "w"
                ) as f:
                    for suite in sorted(versions[ver]):
                        for rest in versions[ver][suite]:
                            f.write(suite + " " + rest + "\n")
            del packages[comp][arch][pkg]
            i += 1
        del packages[comp][arch]
    del packages[comp]
