This is a post-mortem. Maybe it's interesting for somebody and avoids others to
make the same mistakes I did.

This directory contains scripts, that transform the git repository storing the
snapshot.d.o state per timestamp into a view that stores snapshot ranges per
package with one file per package. My idea was to use the filesystem to store
this information because it would make information retrieval possible without
any additional code from any programming language. It would be possible to
browse the information either via a file explorer or offer the content using a
directory listing by a webserver over HTTP without any additional code. If
somebody wanted to download data in bulk, my plan was to just create tarballs
of certain directories and place them next to those directories. As an
additional bonus, files and directories could have their modification time set
to the last timestamp that they contain for even more intuitive browsing. At
this point I didn't even think about storing the information in sqlite. It
turns out, that storing all snapshot.d.o data from 2005 to 2017 takes 115 GB
of disk space in 30 Mill files and directories. So clearly things were getting
out of hand. Since I didn't want to give up, I threw btrfs with compress=zstd
at the problem. Indeed this helped because now the data was only using 34 GB of
space. I was about to be satisfied with this solution when it occurred to me,
that it would be important to organize the data for a different type of query
as well. So far, I had my files organized in a directory structure that was:

    debian/$suite/$component/$architecture/$pkgname

And the last component of the path would be a file listing its versions and
timestamp ranges in each of its lines. But what if somebody wanted to find out
in which suite a specific package version appeared? To avoid having to try all
possible suites, a new view was needed:

    debian/$component/$architecture/$pkgname/$version

And each version would then associate suite names with timestamp ranges where
that specific version appeared in that suite. But of course this new directory
structure will eat another 24 GB of space. Some du output from btrfs:

    $ du -hs disk/*
    74G	disk/by-package
    115G	disk/by-suite
    $ du -s --apparent-size --bytes disk/*
    3986197163	disk/by-package
    4576349767	disk/by-suite

As one can see from the apparent-size, the actually stored data is by more than
a magnitude smaller than what is used as disk space -- probably due to the
additional metadata needed for each file.

At this point I started to reconsider the filesystem idea and started exploring
sqlite.  The naive sqlite version which at every timestamp does a "select"
query to find out whether the package already exists was 20 times slower than
my filesystem based solution. When I replaced the f.write() calls in my more
involved filesystem based script, it was still 10 times slower. I was about to
discard the sqlite idea, when two improvements saved the day:

 1) call commit() much more rarely -- I now do it once per timestamp
 2) instead of issuing a "select" every time, store the id mappings as python
    dicts

To my surprise, it is now possible to store all data that took 58 GB on btrfs
in only FIXME GB of sqlite table. To save space, I added extra tables for suite
names, architecture names, package names, version names and timestamps. That
way, there would be no duplicate strings in the main table. The deduplication
pays off because with 83 Mill rows (timestamps 2005-2017), there are only 118k
unique package names and 201k unique versions. By having the main table only
store integers, the average size per row is 26 bytes.

I tested whether my sqlite version works by comparing it to the btrfs based
solution. This showed another problem that I didn't expect. My btrfs started
failing randomly with errors like this in dmesg:

[972330.999137] INFO: task btrfs-transacti:1132023 blocked for more than 120 seconds.
[972330.999141]       Tainted: G         C        5.9.0-2-amd64 #1 Debian 5.9.6-1
[972330.999141] "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
[972330.999143] task:btrfs-transacti state:D stack:    0 pid:1132023 ppid:     2 flags:0x00004000
[972330.999145] Call Trace:
[972330.999150]  __schedule+0x281/0x8a0
[972330.999153]  ? __sched_text_start+0x8/0x8
[972330.999155]  schedule+0x4a/0xb0
[972330.999157]  schedule_timeout+0x114/0x160
[972330.999158]  io_schedule_timeout+0x4b/0x80
[972330.999160]  __wait_for_common+0xae/0x160
[972330.999180]  write_all_supers+0x8cb/0x990 [btrfs]
[972330.999197]  btrfs_commit_transaction+0x755/0xa30 [btrfs]
[972330.999213]  ? start_transaction+0xd2/0x540 [btrfs]
[972330.999216]  ? try_to_wake_up+0x220/0x5e0
[972330.999230]  transaction_kthread+0x14c/0x170 [btrfs]
[972330.999245]  ? btrfs_cleanup_transaction.isra.0+0x5a0/0x5a0 [btrfs]
[972330.999247]  kthread+0x11b/0x140
[972330.999249]  ? __kthread_bind_mask+0x60/0x60
[972330.999251]  ret_from_fork+0x22/0x30
[972330.999254] INFO: task grep:1132595 blocked for more than 120 seconds.
[972330.999256]       Tainted: G         C        5.9.0-2-amd64 #1 Debian 5.9.6-1
[972330.999257] "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
[972330.999258] task:grep            state:D stack:    0 pid:1132595 ppid:1030795 flags:0x00004004
[972330.999259] Call Trace:
[972330.999262]  __schedule+0x281/0x8a0
[972330.999263]  schedule+0x4a/0xb0
[972330.999265]  io_schedule+0x41/0x70
[972330.999267]  wait_on_page_bit_common+0x116/0x3c0
[972330.999269]  ? dio_warn_stale_pagecache.part.0+0x50/0x50
[972330.999288]  read_extent_buffer_pages+0x369/0x3b0 [btrfs]
[972330.999305]  btree_read_extent_buffer_pages+0x97/0x110 [btrfs]
[972330.999321]  read_tree_block+0x36/0x60 [btrfs]
[972330.999335]  read_block_for_search.isra.0+0x1a3/0x350 [btrfs]
[972330.999338]  ? page_counter_uncharge+0x36/0x50
[972330.999352]  btrfs_search_slot+0x22e/0x970 [btrfs]
[972330.999354]  ? _cond_resched+0x16/0x40
[972330.999369]  btrfs_lookup_inode+0x3a/0xc0 [btrfs]
[972330.999385]  btrfs_read_locked_inode+0x53f/0x620 [btrfs]
[972330.999402]  btrfs_iget_path+0x8d/0xd0 [btrfs]
[972330.999419]  btrfs_lookup_dentry+0x40c/0x540 [btrfs]
[972330.999436]  btrfs_lookup+0xe/0x30 [btrfs]
[972330.999438]  path_openat+0x4fe/0x1100
[972330.999441]  do_filp_open+0x88/0x130
[972330.999443]  ? getname_flags.part.0+0x29/0x1a0
[972330.999445]  ? __check_object_size+0x136/0x150
[972330.999447]  do_sys_openat2+0x97/0x150
[972330.999448]  __x64_sys_openat+0x54/0x90
[972330.999451]  do_syscall_64+0x33/0x80
[972330.999453]  entry_SYSCALL_64_after_hwframe+0x44/0xa9
[972330.999455] RIP: 0033:0x7f454e355cc2
[972330.999456] Code: Unable to access opcode bytes at RIP 0x7f454e355c98.
[972330.999457] RSP: 002b:00007fffab8164f0 EFLAGS: 00000246 ORIG_RAX: 0000000000000101
[972330.999458] RAX: ffffffffffffffda RBX: 0000000000000001 RCX: 00007f454e355cc2
[972330.999459] RDX: 0000000000000100 RSI: 00007fffab8183bc RDI: 00000000ffffff9c
[972330.999459] RBP: 0000000000000001 R08: 000055658bf97480 R09: 00007f454e425be0
[972330.999460] R10: 0000000000000000 R11: 0000000000000246 R12: 00007fffab816780
[972330.999460] R13: 00007fffab816650 R14: 00007fffab8168d8 R15: 0000000000000001
