#!/usr/bin/env python3

# 5G RAM
# 34G disk
# python3 run2.py debian  222226.98s user 13195.67s system 88% cpu 73:34:37.78 total

import sys
import subprocess
import os
from collections import defaultdict
from debian.debian_support import AptPkgVersion
from datetime import datetime
from pathlib import Path

if len(sys.argv) not in (2, 5):
    print("usage: %s archive [onlysuite onlyarea onlyarch]" % sys.argv[0])
    exit(1)
archive = sys.argv[1]
onlysuite = None
onlyarea = None
onlyarch = None
if len(sys.argv) == 5:
    onlysuite, onlyarea, onlyarch = sys.argv[2:5]

timestamps = (
    subprocess.check_output(["git", "-C", "./by-timestamp/%s.git" % archive, "tag"])
    .decode("utf-8")
    .splitlines()
)

# make sure not to accidentally overwrite something
assert not os.path.exists("by-suite/timestamps.txt")

lastversions = defaultdict(dict)
lasttimestamp = None
lastarches = dict()
for timestamp in timestamps:
    print(timestamp)
    subprocess.check_call(
        [
            "git",
            "-C",
            "./by-timestamp/%s.git" % archive,
            "checkout",
            "--quiet",
            timestamp,
        ]
    )
    thisarches = defaultdict(set)
    thisversions = dict()
    for suite in os.listdir("./by-timestamp/%s.git" % archive):
        if suite == ".git":
            continue
        if onlysuite is not None and onlysuite != suite:
            continue
        for area in ["main", "contrib", "non-free"]:
            if onlyarea is not None and onlyarea != area:
                continue
            for arch in os.listdir(
                "./by-timestamp/%s.git/%s/%s" % (archive, suite, area)
            ):
                if onlyarch is not None and onlyarch != arch:
                    continue
                thisarches[suite].add(arch)
                os.makedirs(
                    "by-suite/%s/%s/%s/%s" % (archive, suite, area, arch),
                    exist_ok=True,
                )
                key = suite + " " + area + " " + arch
                thisversions[key] = dict()
                with open(
                    "./by-timestamp/%s.git/%s/%s/%s" % (archive, suite, area, arch),
                    encoding="ascii",
                ) as f:
                    for line in f:
                        pkg, ver = line.rstrip().split(" ", maxsplit=1)
                        # if pkg != "xserver-common":
                        #    continue
                        if pkg in thisversions[key]:
                            thisversions[key][pkg].add(ver)
                        else:
                            thisversions[key][pkg] = set([ver])

                for pkg in thisversions[key]:
                    last_ver = lastversions[key].get(pkg)
                    if timestamp == timestamps[-1]:
                        # special handling for last timestamp
                        with open(
                            "by-suite/%s/%s/%s/%s/%s"
                            % (archive, suite, area, arch, pkg),
                            "a",
                        ) as f:
                            if last_ver is None:
                                for ver in sorted(
                                    thisversions[key][pkg], key=AptPkgVersion
                                ):
                                    f.write("%s %s %s\n" % (ver, timestamp, timestamp))
                            else:
                                if last_ver.keys() == thisversions[key][pkg]:
                                    for ver in sorted(last_ver, key=AptPkgVersion):
                                        f.write(
                                            "%s %s %s\n"
                                            % (ver, last_ver[ver], timestamp)
                                        )
                                else:
                                    # there is a difference
                                    for ver in sorted(
                                        last_ver.keys() | thisversions[key][pkg]
                                    ):
                                        if (
                                            ver in last_ver
                                            and ver in thisversions[key][pkg]
                                        ):
                                            # version is in both, the last and current timestamp
                                            f.write(
                                                "%s %s %s\n"
                                                % (ver, last_ver[ver], timestamp)
                                            )
                                        elif (
                                            ver in last_ver
                                            and ver not in thisversions[key][pkg]
                                        ):
                                            # version is in last but not current timestamp
                                            f.write(
                                                "%s %s %s\n"
                                                % (ver, last_ver[ver], lasttimestamp)
                                            )
                                        elif (
                                            ver not in last_ver
                                            and ver in thisversions[key][pkg]
                                        ):
                                            # version is new in this timestamp
                                            f.write(
                                                "%s %s %s\n"
                                                % (ver, timestamp, timestamp)
                                            )
                                        else:
                                            raise Exception("logic error")
                                    # for ver in sorted(last_ver.keys() - thisversions[key][pkg], key=AptPkgVersion):
                                    #    # for all removed versions, add a new line
                                    #    f.write("%s %s %s\n" % (ver, last_ver[ver], lasttimestamp))
                                    # for ver in sorted(thisversions[key][pkg] & last_ver.keys(), key=AptPkgVersion):
                                    #    # for all versions in the last and current, add a new line
                                    #    f.write("%s %s %s\n" % (ver, last_ver[ver], timestamp))
                                    # for ver in sorted(thisversions[key][pkg] - last_ver.keys(), key=AptPkgVersion):
                                    #    # for all added versions, add a new line
                                    #    f.write("%s %s %s\n" % (ver, timestamp, timestamp))
                        if last_ver is not None:
                            del lastversions[key][pkg]
                        continue
                    if last_ver is None:
                        # package didn't exist before, start new entry
                        lastversions[key][pkg] = {
                            v: timestamp for v in thisversions[key][pkg]
                        }
                        continue
                    if last_ver.keys() == thisversions[key][pkg]:
                        # same versions in this Packages file as in the last
                        continue
                    # there is a difference -- add a new line to the output for
                    # each version that was removed and add a new entry in the
                    # dict for each version that was added
                    with open(
                        "by-suite/%s/%s/%s/%s/%s" % (archive, suite, area, arch, pkg),
                        "a",
                    ) as f:
                        for ver in sorted(
                            last_ver.keys() - thisversions[key][pkg], key=AptPkgVersion
                        ):
                            # for all removed versions, add a new line
                            f.write("%s %s %s\n" % (ver, last_ver[ver], lasttimestamp))
                            # and delete the old entry
                            del lastversions[key][pkg][ver]
                    for ver in sorted(
                        thisversions[key][pkg] - last_ver.keys(), key=AptPkgVersion
                    ):
                        # for all added versions, add a new entry
                        lastversions[key][pkg][ver] = timestamp

                # go through all packages that were handled last timestamp but
                # were missing this timestamp
                for pkg in lastversions[key].keys() - thisversions[key].keys():
                    with open(
                        "by-suite/%s/%s/%s/%s/%s" % (archive, suite, area, arch, pkg),
                        "a",
                    ) as f:
                        for ver in sorted(lastversions[key][pkg], key=AptPkgVersion):
                            f.write(
                                "%s %s %s\n"
                                % (
                                    ver,
                                    lastversions[key][pkg][ver],
                                    lasttimestamp,
                                )
                            )
                    del lastversions[key][pkg]
    # add entries for all suites that were present in the last timestamp
    # but not anymore in this timestamp
    for suite in lastarches.keys() - thisarches.keys():
        for arch in lastarches[suite]:
            for area in ["main", "contrib", "non-free"]:
                if onlyarea is not None and onlyarea != area:
                    continue
                key = suite + " " + area + " " + arch
                for pkg in lastversions[key]:
                    with open(
                        "by-suite/%s/%s/%s/%s/%s" % (archive, suite, area, arch, pkg),
                        "a",
                    ) as f:
                        for ver in sorted(lastversions[key][pkg], key=AptPkgVersion):
                            f.write(
                                "%s %s %s\n"
                                % (
                                    ver,
                                    lastversions[key][pkg][ver],
                                    lasttimestamp,
                                )
                            )
                del lastversions[key]
    # handle architectures that were present in the last timestamp but not
    # anymore in this one
    for suite in lastarches.keys() & thisarches.keys():
        for arch in lastarches[suite] - thisarches[suite]:
            for area in ["main", "contrib", "non-free"]:
                if onlyarea is not None and onlyarea != area:
                    continue
                key = suite + " " + area + " " + arch
                for pkg in lastversions[key]:
                    with open(
                        "by-suite/%s/%s/%s/%s/%s" % (archive, suite, area, arch, pkg),
                        "a",
                    ) as f:
                        for ver in sorted(lastversions[key][pkg], key=AptPkgVersion):
                            f.write(
                                "%s %s %s\n"
                                % (
                                    ver,
                                    lastversions[key][pkg][ver],
                                    lasttimestamp,
                                )
                            )
                del lastversions[key]
    lastarches = thisarches
    lasttimestamp = timestamp

# this removes empty suites like oldoldstable-updates, oldstable-lts,
# squeeze-volatile, stretch-updates, testing-updates
for root, dirs, files in os.walk("by-suite", topdown=False):
    # skip, if any files are present
    if files:
        continue
    # since we are removing directories, we have to call listdir again
    if os.listdir(root):
        continue
    os.rmdir(root)

for archive in Path("by-suite").iterdir():
    if not archive.is_dir():
        assert archive.name == "timestamps.txt"
        continue
    latest_suitetimestamp = 0
    for suite in archive.iterdir():
        print(suite)
        latest_comptimestamp = 0
        for comp in suite.iterdir():
            assert comp.name in ["main", "contrib", "non-free"]
            latest_archtimestamp = 0
            for arch in comp.iterdir():
                # if not arch.is_dir():
                #    assert arch.suffixes == ['.tar', '.xz']
                #    continue
                latest_pkgtimestamp = 0
                for pkg in arch.iterdir():
                    assert pkg.is_file()
                    last_line = pkg.read_text().splitlines()[-1]
                    _, _, timestamp = last_line.split(" ")
                    timestamp = datetime.strptime(
                        timestamp, "%Y%m%dT%H%M%S%z"
                    ).timestamp()
                    os.utime(pkg, times=(timestamp, timestamp))
                    if timestamp > latest_pkgtimestamp:
                        latest_pkgtimestamp = timestamp
                os.utime(arch, times=(latest_pkgtimestamp, latest_pkgtimestamp))
                # subprocess.check_call(
                #    [
                #        "tar",
                #        "--xz",
                #        "--directory",
                #        arch,
                #        "--create",
                #        "--file",
                #        arch.with_suffix(".tar.xz"),
                #        ".",
                #    ],
                #    env={"XZ_OPT": "-9e"},
                # )
                # os.utime(arch.with_suffix(".tar.xz"), times=(latest_pkgtimestamp, latest_pkgtimestamp))
                if latest_pkgtimestamp > latest_archtimestamp:
                    latest_archtimestamp = latest_pkgtimestamp
            os.utime(comp, times=(latest_archtimestamp, latest_archtimestamp))
            if latest_archtimestamp > latest_comptimestamp:
                latest_comptimestamp = latest_archtimestamp
        os.utime(suite, times=(latest_comptimestamp, latest_comptimestamp))
        if latest_comptimestamp > latest_suitetimestamp:
            latest_comptimestamp = latest_suitetimestamp
    os.utime(archive, times=(latest_suitetimestamp, latest_suitetimestamp))

with open("by-suite/timestamps.txt", "w") as f:
    for timestamp in timestamps:
        f.write(timestamp + "\n")
