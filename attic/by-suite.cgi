#!/usr/bin/env python3

import os
import urllib.parse
from pathlib import Path
from operator import itemgetter

doc_root = os.environ['DOCUMENT_ROOT']
qs = urllib.parse.parse_qs(os.environ['QUERY_STRING'])

if 'build-arch' not in qs:
    print("Status: 400 Bad Request")
    print("X-Explanation: need the build-arch value")
    print()
    exit(1)

if 'packages' not in qs:
    print("Status: 400 Bad Request")
    print("X-Explanation: need the packages value")
    print()
    exit(1)

ranges = list()
for t in qs['packages'][0].split(","):
    name, ver = t.split("=", maxsplit=1)
    arch = qs["build-arch"][0]
    if ":" in name:
        name, arch = name.split(":", maxsplit=1)
    fname = Path(doc_root) / "by-package" / "debian" / "unstable" / "main" / arch / name
    if not fname.exists():
        print("Status: 400 Bad Request")
        print("X-Explanation: cannot find package for %s" % line)
        print()
        exit(1)
    r = list()
    with open(fname, "r") as f:
        for l in f:
            l = l.strip()
            v, s, e = l.split()
            if v != ver:
                continue
            r.append((s, e))
    if len(r) == 0:
        print("Status: 400 Bad Request")
        print("X-Explanation: version not found for %s" % line)
        print()
        exit(1)
    if len(r) > 1:
        print("Status: 400 Bad Request")
        print("X-Explanation: multiple ranges found for %s" % line)
        print()
        exit(1)
    ranges.append(r[0])

# This algorithm is similar to Interval Scheduling
# https://en.wikipedia.org/wiki/Interval_scheduling
# But instead of returning the ranges, we return the endtime of ranges
# See also:
# https://stackoverflow.com/questions/27753830/
# https://stackoverflow.com/questions/4962015/
# https://cs.stackexchange.com/questions/66376/
# https://stackoverflow.com/questions/52137509/
# https://www.codechef.com/problems/ONEKING
# https://discuss.codechef.com/t/oneking-editorial/9096
ranges.sort(key=itemgetter(1))
result = list()
last = "19700101T000000Z"  # impossibly early date
for b, e in ranges:
    if last >= b:
        continue
    last = e
    result.append(last)
print("Content-type: text/plain")
print()
print("\n".join(result))
