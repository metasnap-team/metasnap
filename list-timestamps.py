#!/usr/bin/env python3

import urllib.request
import json
from pathlib import Path
import sys

outdir = Path("by-timestamp")
outdir.mkdir(exist_ok=True)

if len(sys.argv) != 2:
    print("usage: %s archive" % sys.argv[0])
    exit(1)

archive = sys.argv[1]

assert archive in [
    "debian",
    "debian-backports",
    "debian-debug",
    "debian-ports",
    "debian-security",
    "debian-volatile",
]

timestamps = []
outfile = (outdir / archive).with_suffix(".txt")
if outfile.exists():
    timestamps = outfile.read_text().splitlines()

with urllib.request.urlopen(
    f"http://snapshot.debian.org/mr/timestamp?archive={archive}"
) as f:
    data = json.loads(f.read())["result"][archive]

# make sure that the result we get starts with the timestamps we already have
assert data[: len(timestamps)] == timestamps

outfile.write_text("\n".join(data) + "\n")
