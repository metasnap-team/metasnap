def skip_timestamp(archive, timestamp):
    if archive == "debian-debug":
        if timestamp == "20151224T161549Z":
            # the /dists directory is completely missing
            return True
    elif archive == "debian-ports":
        if timestamp in [
            "20101022T004605Z",
            "20101023T004623Z",
            "20101023T063838Z",
            "20110816T162326Z",
        ]:
            # all Release files are zero size
            return True
        elif "20110817T153635Z" <= timestamp == "20110817T171230Z":
            # the Release files reference the compressed Packages files but not
            # the uncompressed Packages files
            return True
        elif timestamp == "20130608T131816Z":
            # wrong checksums and Packages files with empty Package field
            return True
        elif "20130609T005948Z" <= timestamp <= "20130609T005948Z":
            # partial bz2 files and empty package names
            return True
        elif "20130808T183613Z" <= timestamp <= "20130809T002832Z":
            # empty Packages files
            # partial Packages file
            # release file does not contain Packages
            # missing hashes
            return True
        elif timestamp == "20140508T073813Z":
            # broken Packages files
            return True
        elif timestamp == "20140805T073551Z":
            # partial Packages file
            return True
        elif timestamp == "20140805T191528Z":
            # partial Packages file
            return True
        elif timestamp == "20140905T134333Z":
            # partial Packages file
            return True
        elif timestamp == "20141013T032127Z":
            # partial Packages file
            return True
        elif "20150530T033811Z" <= timestamp <= "20150601T072651Z":
            # partial Packages file
            return True
        elif timestamp == "20150708T070238Z":
            # partial Packages file
            return True
        elif "20151130T134116Z" <= timestamp <= "20151130T193543Z":
            # partial Packages file
            return True
        elif timestamp == "20160321T204606Z":
            # partial Packages file
            return True
        elif timestamp == "20160420T141838Z":
            # Packages file missing from Release file
            return True
    return False


def validate_suite_dir(archive, timestamp, suites):
    assert len(suites) > 0
    if archive == "debian":
        assert "sid" in suites
    if (
        archive == "debian"
        and timestamp >= "20160118T221907Z" <= timestamp <= "20160119T221256Z"
    ):
        suites.remove("proposed-updates-proposed-updates")


def validate_suite_link(archive, timestamp, suites, dst, src):
    if archive == "debian":
        if src == "../project/experimental":
            return None
    elif archive == "debian-security":
        if (
            "20081030T000000Z" <= timestamp <= "20081109T000000Z"
            and dst == "oldstable"
            and src == "sarge"
        ):
            # there is a broken symlink from oldstable to sarge
            assert "sarge" not in suites
            return None
    if archive == "debian-volatile":
        if src.endswith("/volatile"):
            src = src.removesuffix("/volatile")
            assert src in suites, src
        elif src.endswith("-volatile"):
            assert src.removesuffix("-volatile") in suites, src
    else:
        assert src in suites, src
    return src


def validate_suite(archive, timestamp, suite):
    if archive == "debian":
        # explanation for the r0 directories:
        # 2023-06-13
        # 20:14 < adsb> they get updated on every point release
        # 20:14 < adsb> well, the ones for supported (old)stable suites do
        # 20:15 < adsb> adsb@coccia:~$ dak admin s-cfg get-value bookworm-r0
        #               description
        # 20:15 < adsb> Extraneous packages/sources required for GPL compliance
        # 20:16 < adsb> mostly superseded by built-using now, but still used
        #               for keeping released versions of d-i
        # 20:19 < adsb> maybe snapshot doesn't import the content? although
        #               does feel odd that it's created the empty directories
        # 20:21 < ansgar> adsb: dak init-dirs created the directories as it
        #                 probably doesn't look at the setting that makes
        #                 other parts not generate Packages/Source/Release/* for it.
        # 20:21 < kibi> data point: no r0 in the code
        # 20:25 < adsb> it could probably be cleaned up as we go I guess, but
        if timestamp == "20190706T233220Z" and suite in [
            "bullseye-backports",
            "stretch-backports-sloppy",
        ]:
            # release files are 404
            return False
        elif timestamp == "20210814T212851Z" and suite in [
            "bullseye-backports-sloppy",
            "bullseye-r0",
            "buster-r0",
            "stretch-r0",
            "jessie-r0",
        ]:
            # empty directories
            return False
        elif timestamp == "20230610T205311Z" and suite in [
            "bookworm-backports-sloppy",
            "bookworm-r0",
            "buster-r0",
            "bullseye-r0",
        ]:
            # empty directories
            return False
    elif archive == "debian-debug":
        if timestamp == "20190706T233208Z" and suite in [
            "bullseye-backports-debug",
            "bullseye-proposed-updates-debug",
            "buster-backports-debug",
            "stretch-backports-sloppy-debug",
        ]:
            # release files are 404
            return False
        elif timestamp == "20210814T212408Z" and suite in [
            "stable-backports-sloppy-debug",
            "testing-proposed-updates-debug",
        ]:
            # empty directories
            return False
        elif "20230610T195249Z" <= timestamp <= "20230610T205516Z" and suite in [
            "stable-backports-sloppy-debug",
            "testing-backports-debug",
            "testing-proposed-updates-debug",
        ]:
            # release files are 404
            return False
    elif archive == "debian-security":
        if suite == "slink":
            # release files are 404
            return False
        elif (
            "20190406T171942Z" <= timestamp <= "20190407T124526Z"
            and suite == "oldoldstable"
        ):
            return False
    elif archive == "debian-volatile":
        if "20070329T000000Z" <= timestamp <= "20070331T000000Z" and suite in [
            "etch",
            "etch-proposed-updates",
        ]:
            # release file signature is 404
            return False
    elif archive == "debian-ports":
        if timestamp == "20111004T055315Z" and suite == "unstable":
            # release file is 404
            return False
        elif timestamp == "20130607T121939Z" and suite == "unreleased":
            # release file does not contain Packages files
            return False
        elif timestamp == "20130608T002420Z" and suite == "unreleased":
            # release file does not contain Packages files
            return False
        elif timestamp == "20130807T190423Z" and suite == "experimental":
            # partial Packages file
            return False
        elif timestamp == "20130808T011350Z" and suite == "unstable":
            # partial Packages file
            return False
        elif timestamp == "20130808T071905Z" and suite in ["experimental", "unstable"]:
            # partial Packages file and missing hashes
            return False
        elif (
            "20131114T061813Z" <= timestamp <= "20131114T111803Z"
            and suite == "unreleased"
        ):
            # the Packages files are not in the Release file
            return False
        elif timestamp == "20140307T121810Z" and suite == "unreleased":
            # the Packages files are not in the Release file
            return False
        elif timestamp == "20221006T135624Z" and suite in ["experimental", "unstable"]:
            # Packages files are 404
            return False
    return True


def skip_verify0(archive, timestamp, suite):
    skip_verify0 = False
    if archive == "debian":
        if timestamp == "20060811T000000Z" and suite == "experimental":
            skip_verify0 = True
        elif timestamp == "20091101T033049Z" and suite in [
            "etch-proposed-updates",
            "experimental",
            "lenny-proposed-updates",
            "sid",
            "squeeze",
            "squeeze-proposed-updates",
        ]:
            skip_verify0 = True
        elif timestamp == "20101003T150533Z" and suite in [
            "experimental",
            "lenny-proposed-updates",
            "sid",
            "squeeze",
            "squeeze-proposed-updates",
            "squeeze-volatile",
        ]:
            skip_verify0 = True
        elif timestamp == "20110221T213249Z" and suite == "wheezy":
            skip_verify0 = True
        elif timestamp == "20110312T103459Z" and suite == "sid":
            skip_verify0 = True
        elif timestamp == "20110312T142115Z" and suite in [
            "experimental",
            "lenny-proposed-updates",
            "sid",
            "squeeze-proposed-updates",
            "squeeze-updates",
            "wheezy",
            "wheezy-proposed-updates",
        ]:
            skip_verify0 = True
        elif (
            "20170721T032226Z" <= timestamp <= "20170729T033133Z"
            and suite == "wheezy-proposed-updates"
        ):
            skip_verify0 = True
        elif (
            "20190407T215605Z" <= timestamp <= "20190408T024815Z"
            and suite == "buster-backports"
        ):
            skip_verify0 = True
    elif archive == "debian-backports":
        if "20101003T134140Z" <= timestamp <= "20101003T143836Z" and suite in [
            "lenny-backports",
            "lenny-backports-sloppy",
        ]:
            # the gpg signature has zero size
            skip_verify0 = True
    elif archive == "debian-security":
        if timestamp == "20091108T121453Z":
            skip_verify0 = True
    elif archive == "debian-ports":
        if "20110131T130004Z" <= timestamp <= "20110131T185617Z" and suite in [
            "experimental",
            "unreleased",
            "unstable",
        ]:
            # gpg signature doesn't verify
            skip_verify0 = True
        elif timestamp == "20130131T205328Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif "20130929T233138Z" <= timestamp <= "20131001T132840Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif timestamp == "20140131T132248Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif timestamp == "20150131T193304Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif "20160201T073956Z" <= timestamp <= "20160202T073346Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif "20180201T200844Z" <= timestamp <= "20180202T075715Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif timestamp == "20200131T230024Z":
            # gpg signature doesn't verify
            skip_verify0 = True
        elif timestamp == "20220201T020307Z":
            # gpg signature doesn't verify
            skip_verify0 = True
    return skip_verify0


def validate_components(archive, timestamp, suite, components):
    if archive == "debian":
        if (
            "20190404T213121Z" <= timestamp <= "20190706T025754Z"
            and suite == "jessie-updates"
        ):
            assert components == ""
            components = "main contrib non-free"
        if timestamp < "20220718T213229Z":
            assert components == "main contrib non-free"
        elif suite in [
            "bullseye",
            "bullseye-backports",
            "bullseye-backports-sloppy",
            "bullseye-proposed-updates",
            "bullseye-updates",
            "buster",
            "buster-backports",
            "buster-backports-sloppy",
            "buster-proposed-updates",
            "buster-updates",
            "stretch",
            "stretch-backports",
            "stretch-backports-sloppy",
            "stretch-proposed-updates",
            "stretch-updates",
            "jessie",
            "jessie-updates",
        ]:
            assert components == "main contrib non-free"
        else:
            assert components == "main contrib non-free-firmware non-free"
    elif archive == "debian-security":
        if "20121207T155715Z" <= timestamp <= "20130514T193534Z":
            assert (
                components
                == "updates/updates/main updates/updates/contrib updates/updates/non-free"
            )
            components = "updates/main updates/contrib updates/non-free"
        if "20220722T121349Z" <= timestamp and suite in ["bookworm-security"]:
            assert (
                components
                == "updates/main updates/contrib updates/non-free-firmware updates/non-free"
            )
            components = "main contrib non-free-firmware non-free"
        elif timestamp < "20230610T194030Z":
            assert (
                components == "updates/main updates/contrib updates/non-free"
            ), components
            components = "main contrib non-free"
        elif suite in ["bullseye-security", "buster"]:
            assert (
                components == "updates/main updates/contrib updates/non-free"
            ), components
            components = "main contrib non-free"
        else:
            assert (
                components
                == "updates/main updates/contrib updates/non-free-firmware updates/non-free"
            ), components
            components = "main contrib non-free-firmware non-free"
    elif archive == "debian-debug":
        if timestamp < "20220718T210850Z":
            assert components == "main contrib non-free"
        elif suite in [
            "bullseye-backports-debug",
            "bullseye-backports-sloppy-debug",
            "bullseye-debug",
            "bullseye-proposed-updates-debug",
            "buster-backports-debug",
            "buster-backports-sloppy-debug",
            "buster-debug",
            "buster-proposed-updates-debug",
            "jessie-debug",
            "stretch-backports-debug",
            "stretch-backports-sloppy-debug",
            "stretch-debug",
            "stretch-proposed-updates-debug",
        ]:
            assert components == "main contrib non-free"
        else:
            assert components == "main contrib non-free-firmware non-free"
    elif archive == "debian-ports":
        if "20230219T222402Z" <= timestamp:
            assert components == "main contrib non-free non-free-firmware"
        else:
            # even though more components were listed in the Release file,
            # for the longest time, only "main" contained anything
            return ["main"]
    return components.split()


def component_is_404(archive, timestamp, suite, component):
    if (
        timestamp == "20220718T213229Z"
        and archive == "debian"
        and component == "non-free-firmware"
        and suite
        in [
            "bookworm",
            "bookworm-backports",
            "bookworm-proposed-updates",
            "bookworm-updates",
            "experimental",
            "sid",
        ]
    ):
        return True
    if (
        timestamp == "20220718T210850Z"
        and archive == "debian-debug"
        and component == "non-free-firmware"
        and suite
        in [
            "bookworm-backports-debug",
            "bookworm-debug",
            "bookworm-proposed-updates-debug",
            "rc-buggy-debug",
            "sid-debug",
        ]
    ):
        return True
    return False


def validate_release_arches(archive, timestamp, suite, release_arches, release_files):
    if archive == "debian":
        if timestamp == "20070408T000000Z" and suite == "etch-proposed-updates":
            assert "amd64" not in release_arches
            for arch in ["amd64"]:
                for area in ["main", "contrib", "non-free"]:
                    for ext in ["", ".bz2"]:
                        key = "%s/binary-%s/Packages%s" % (area, arch, ext)
                        assert key not in release_files
                        release_files[key] = {
                            "size": -1,
                            "area": area,
                            "arch": arch,
                        }
            release_arches.extend(["amd64"])
        if (
            "20070408T000000Z" <= timestamp <= "20080725T000000Z"
            and suite == "lenny-proposed-updates"
        ):
            # since 20070408T000000Z (the introduction of lenny), the suite
            # lenny-proposed-updates has m68k in the Architecture field of its Release
            # file but no entries for Packages files (and none are archived either)
            release_arches.remove("m68k")
        elif timestamp == "20081124T000000Z" and suite in ["experimental", "sid"]:
            # the Release file lists m68k in Architectures and lists Packages files but those are 404
            release_arches.remove("m68k")
        elif "20090220T030650Z" <= timestamp <= "20090224T160605Z":
            # the Release file has an empty Architecture field
            if suite in [
                "etch-m68k",
                "etch-proposed-updates",
                "experimental",
                "lenny-proposed-updates",
                "sid",
                "squeeze",
                "squeeze-proposed-updates",
            ]:
                assert release_arches == []
            if suite == "etch-m68k":
                release_arches.extend(["m68k"])
            elif suite == "etch-proposed-updates":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
            elif suite in ["sid", "experimental"]:
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "armel",
                        "hppa",
                        "hurd-i386",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
            elif suite == "lenny-proposed-updates":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "armel",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
            elif suite in ["squeeze", "squeeze-proposed-updates"]:
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "armel",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
        elif timestamp == "20101003T163248Z" and suite == "sid":
            # the Release file stores different sizes for sha256
            for arch in ["s390", "kfreebsd-i386", "kfreebsd-amd64"]:
                #assert arch not in release_arches, arch
                #release_arches.append(arch)
                release_arches.remove(arch)
        elif timestamp == "20110312T103459Z" and suite == "sid":
            # the Release file is missing entries for Packages files
            for arch in release_arches:
                for area in ["main", "contrib", "non-free"]:
                    # main/binary-alpha/Packages.bz2 is the only existing entry
                    if arch == "alpha" and area == "main":
                        continue
                    for ext in ["", ".bz2"]:
                        key = "%s/binary-%s/Packages%s" % (area, arch, ext)
                        assert key not in release_files
                        release_files[key] = {
                            "size": -1,
                            "area": area,
                            "arch": arch,
                        }
        elif "20110514T214653Z" <= timestamp <= "20110516T091647Z" and suite in [
            "experimental",
            "sid",
        ]:
            assert "alpha" not in release_arches
            assert "hppa" not in release_arches
            release_arches.extend(["alpha", "hppa"])
        elif (
            "20141227T220359Z" <= timestamp <= "20190321T212815Z"
            and suite == "jessie-backports"
        ):
            assert "sparc" not in release_arches
            release_arches.append("sparc")
        elif (
            "20190324T160900Z" <= timestamp <= "20190706T025754Z" and suite == "jessie"
        ):
            for arch in ["arm64", "mips", "mipsel", "powerpc", "ppc64el", "s390x"]:
                release_arches.remove(arch)
        elif (
            "20190404T213121Z" <= timestamp <= "20190708T153325Z"
            and suite == "jessie-updates"
        ):
            # no Packages files
            for arch in ["amd64", "armel", "armhf", "i386"]:
                release_arches.remove(arch)
        elif "20210814T212851Z" == timestamp and suite == "bullseye-updates":
            release_arches.remove("mips")
        elif "20240413T211957Z" <= timestamp and suite in [
            "buster",
            "buster-proposed-updates",
            "buster-updates",
        ]:
            for arch in ["armel", "mips", "mips64el", "mipsel", "ppc64el", "s390x"]:
                release_arches.remove(arch)
    elif archive == "debian-backports":
        if (
            "20090218T144604Z" <= timestamp <= "20090219T214129Z"
            and suite == "lenny-backports"
        ):
            assert "hurd-i386" not in release_arches
            release_arches.append("hurd-i386")
        elif "20090220T054132Z" <= timestamp <= "20090225T204518Z":
            assert release_arches == []
            if suite == "etch-backports":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "hppa",
                        "hurd-i386",
                        "i386",
                        "ia64",
                        "m68k",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sh",
                        "sparc",
                    ]
                )
            elif suite == "lenny-backports":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "armel",
                        "hppa",
                        "hurd-i386",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
        elif "20090225T214556Z" <= timestamp and suite == "lenny-backports":
            assert "hurd-i386" not in release_arches
            release_arches.append("hurd-i386")
        elif (
            "20100905T223114Z" <= timestamp <= "20110204T194307Z"
            and suite == "squeeze-backports"
        ):
            for arch in ["arm", "hurd-i386"]:
                assert arch not in release_arches
                release_arches.append(arch)
        elif "20101002T153717Z" <= timestamp and suite == "lenny-backports-sloppy":
            assert "hurd-i386" not in release_arches
            release_arches.append("hurd-i386")
        elif (
            "20110324T202012Z" <= timestamp <= "20130316T163750Z"
            and suite == "squeeze-backports"
        ):
            assert "hurd-i386" not in release_arches
            release_arches.append("hurd-i386")
        elif "20160309T034844Z" <= timestamp and suite == "squeeze-backports":
            assert release_arches == []
            release_arches.extend(
                [
                    "amd64",
                    "armel",
                    "i386",
                    "ia64",
                    "kfreebsd-amd64",
                    "kfreebsd-i386",
                    "mips",
                    "mipsel",
                    "powerpc",
                    "s390",
                    "sparc",
                ]
            )
        elif "20160309T034844Z" <= timestamp and suite == "squeeze-backports-sloppy":
            assert release_arches == []
            release_arches.extend(
                [
                    "amd64",
                    "armel",
                    "i386",
                    "ia64",
                    "kfreebsd-amd64",
                    "kfreebsd-i386",
                    "mips",
                    "mipsel",
                    "powerpc",
                    "s390",
                    "sparc",
                ]
            )
    elif archive == "debian-debug":
        if (
            "20161107T031643Z" <= timestamp <= "20170603T151311Z"
            and suite == "stretch-debug"
        ):
            assert "powerpc" not in release_arches
            release_arches.append("powerpc")
        elif (
            "20161107T031643Z" <= timestamp
            and suite == "stretch-proposed-updates-debug"
        ):
            assert "powerpc" not in release_arches
            release_arches.append("powerpc")
        elif "20240413T212756Z" <= timestamp and suite in [
            "buster-debug",
            "buster-proposed-updates-debug",
        ]:
            for arch in ["armel", "mips", "mips64el", "mipsel", "ppc64el", "s390x"]:
                release_arches.remove(arch)
    elif archive == "debian-ports":
        if suite == "unreleased":
            release_arches.remove("source")
        if "20100812T075555Z" <= timestamp <= "20120612T131013Z" and suite in [
            "unstable",
            "experimental",
        ]:
            assert "hurd-i386" not in release_arches
            release_arches.append("hurd-i386")
        if "20111116T033810Z" <= timestamp <= "20120108T141354Z":
            assert "avr32" not in release_arches
            release_arches.append("avr32")
        if "20111214T172337Z" <= timestamp <= "20120402T143901Z":
            assert "s390x" not in release_arches
            release_arches.append("s390x")
        if "20111224T143859Z" <= timestamp <= "20120304T221230Z":
            assert "armhf" not in release_arches
            release_arches.append("armhf")
        if (
            "20141209T125301Z" <= timestamp <= "20141209T182928Z"
            and suite == "unreleased"
        ):
            for arch in ["ppc64", "sh4", "sparc64", "x32"]:
                release_arches.remove(arch)
        if timestamp == "20141209T182928Z" and suite == "unreleased":
            for arch in ["hurd-i386", "m68k", "powerpcspe"]:
                release_arches.remove(arch)
    elif archive == "debian-security":
        if "20050812T000000Z" <= timestamp <= "20060607T000000Z" and suite == "etch":
            # Architecture field lists amd64 but there are no Packages files
            release_arches.remove("amd64")
        elif timestamp == "20051009T000000Z" and suite == "potato":
            # the Packages files are 404
            for arch in ["alpha", "arm", "i386", "m68k", "powerpc", "sparc"]:
                release_arches.remove(arch)
        elif "20090222T121238Z" <= timestamp <= "20090308T193012Z":
            # empty Architecture field
            assert release_arches == []
            if suite == "etch":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
            elif suite == "lenny":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "arm",
                        "armel",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
            elif suite == "squeeze":
                release_arches.extend(
                    [
                        "alpha",
                        "amd64",
                        "armel",
                        "hppa",
                        "i386",
                        "ia64",
                        "mips",
                        "mipsel",
                        "powerpc",
                        "s390",
                        "sparc",
                    ]
                )
        elif "20110206T001511Z" <= timestamp <= "20110324T112420Z" and suite == "lenny":
            for arch in ["kfreebsd-amd64", "kfreebsd-i386"]:
                assert arch not in release_arches
                for area in ["main", "contrib", "non-free"]:
                    for ext in ["", ".bz2"]:
                        key = "%s/binary-%s/Packages%s" % (area, arch, ext)
                        assert key not in release_files
                        release_files[key] = {
                            "size": -1,
                            "area": area,
                            "arch": arch,
                        }
                release_arches.append(arch)
        elif (
            "20110206T001511Z" <= timestamp <= "20110324T112420Z" and suite == "squeeze"
        ):
            for arch in ["alpha", "hppa"]:
                assert arch not in release_arches
                for area in ["main", "contrib", "non-free"]:
                    for ext in ["", ".bz2"]:
                        key = "%s/binary-%s/Packages%s" % (area, arch, ext)
                        assert key not in release_files
                        release_files[key] = {
                            "size": -1,
                            "area": area,
                            "arch": arch,
                        }
                release_arches.append(arch)
        elif (
            "20110206T001511Z" <= timestamp <= "20110324T112420Z" and suite == "wheezy"
        ):
            for arch in ["alpha", "hppa", "arm"]:
                assert arch not in release_arches
                for area in ["main", "contrib", "non-free"]:
                    for ext in ["", ".bz2"]:
                        key = "%s/binary-%s/Packages%s" % (area, arch, ext)
                        assert key not in release_files
                        release_files[key] = {
                            "size": -1,
                            "area": area,
                            "arch": arch,
                        }
                release_arches.append(arch)
        elif "20160710T122332Z" == timestamp and suite == "stretch":
            release_arches.remove("mips64el")
    elif archive == "debian-volatile":
        if "20050313T000000Z" <= timestamp <= "20050602T000000Z":
            if suite in ["sarge", "sarge-proposed-updates"]:
                release_arches.remove("hurd-i386")
            release_arches.remove("sh")


def validate_Packages(archive, timestamp, suite, area, arch, bname):
    if archive == "debian":
        if (
            timestamp == "20081124T000000Z"
            and suite in ["experimental", "sid"]
            and arch == "binary-m68k"
        ):
            # the Packages files are 404
            return False
        elif (
            timestamp == "20101003T163248Z"
            and suite == "sid"
            and arch in ["binary-s390", "binary-kfreebsd-i386", "binary-kfreebsd-amd64"]
        ):
            # the Release file stores different sizes for sha256
            return False
        elif (
            "20161107T033615Z" <= timestamp  # FIXME: find last
            and suite in ["stretch", "stretch-proposed-updates", "stretch-updates"]
            and area in ["contrib", "non-free"]
            and arch == "binary-powerpc"
        ):
            # powerpc is missing from the Architectures field, there
            # is still a non-free Packages file but the others are missing
            return False
        elif (
            "20190324T160900Z" <= timestamp  # FIXME: find last
            and suite == "jessie"
            and arch
            in [
                "binary-arm64",
                "binary-mips",
                "binary-mipsel",
                "binary-powerpc",
                "binary-ppc64el",
                "binary-s390x",
            ]
        ):
            # the Packages files are 404
            return False
        elif (
            "20210814T212851Z" == timestamp
            and suite == "bullseye-updates"
            and arch == "binary-mips"
        ):
            # the Packages files are 404
            return False
        elif (
            "20230902T204331Z" <= timestamp
            and suite == "trixie-proposed-updates"
            and arch == "binary-mipsel"
        ):
            # mipsel is no longer in the Architectures field of Release
            # but still has zero-sized main/binary-mipsel/Packages
            return False
        elif (
            "20240413T211957Z" <= timestamp  # FIXME: find last
            and suite in ["buster", "buster-proposed-updates", "buster-updates"]
            and arch
            in [
                "binary-armel",
                "binary-mips",
                "binary-mips64el",
                "binary-mipsel",
                "binary-ppc64el",
                "binary-s390x",
            ]
        ):
            # the Packages files are 404
            return False
    if archive == "debian-debug":
        if (
            "20240413T212756Z" <= timestamp  # FIXME: find last
            and suite in ["buster-debug", "buster-proposed-updates-debug"]
            and arch
            in [
                "binary-armel",
                "binary-mips",
                "binary-mips64el",
                "binary-mipsel",
                "binary-ppc64el",
                "binary-s390x",
            ]
        ):
            # the Packages files are 404
            return False
    elif archive == "debian-ports":
        if (
            "20120612T151839Z" <= timestamp <= "20190421T104122Z"
            and suite in ["experimental", "unstable"]
            and arch == "binary-hurd-i386"
        ):
            # the Packages file is not in the Release file
            return False
        if timestamp == "20121225T133343Z" and arch == "binary-x32":
            # the Packages file is 404
            return False
        if "20141107T013310Z" <= timestamp and arch == "binary-arm64":
            # the Packages file is not in the Release file
            return False
        if (
            "20141209T125301Z" <= timestamp <= "20141209T182928Z"
            and suite == "unreleased"
            and arch in ["binary-ppc64", "binary-sh4", "binary-sparc64", "binary-x32"]
        ):
            # the Packages file is not in the Release file
            return False
        if (
            timestamp == "20141209T182928Z"
            and suite == "unreleased"
            and arch in ["binary-hurd-i386", "binary-m68k", "binary-powerpcspe"]
        ):
            # the Packages file is not in the Release file
            return False
    elif archive == "debian-security":
        if (
            timestamp == "20051009T000000Z"
            and suite == "potato"
            and arch
            in [
                "binary-alpha",
                "binary-arm",
                "binary-i386",
                "binary-m68k",
                "binary-powerpc",
                "binary-sparc",
            ]
        ):
            # the Packages files are 404
            return False
    return True


def validate_Packages_link(archive, timestamp, suite, area, arch, release_arches):
    if archive == "debian":
        if (
            timestamp == "20060113T000000Z"
            and suite in ["sid", "experimental"]
            and arch == "binary-sh"
        ):
            # sh is not present in the Release file and directory only
            # contains an empty Packages.diff
            return False
        elif (
            timestamp == "20070408T000000Z"
            and suite in ["etch", "etch-proposed-updates"]
            and arch == "binary-m68k"
        ):
            return False
        elif (
            timestamp == "20101003T163248Z"
            and suite == "sid"
            and arch in ["binary-s390", "binary-kfreebsd-i386", "binary-kfreebsd-amd64"]
        ):
            # the Release file stores different sizes for sha256
            return False
        elif (
            "20161107T033615Z" <= timestamp  # FIXME: find last
            and suite in ["stretch", "stretch-proposed-updates", "stretch-updates"]
            and area in ["contrib", "non-free"]
            and arch == "binary-powerpc"
        ):
            # powerpc is missing from the Architectures field, there
            # is still a non-free Packages file but the others are missing
            assert "powerpc" not in release_arches
            return False
        elif (
            "20230610T205311Z" <= timestamp <= "20230611T103552Z"
            and suite == "buster-proposed-updates"
            and arch == "binary-mips"
        ):
            # the Packages file is not in the Release file
            assert "mips" not in release_arches
            return False
        elif (
            "20230902T204331Z" <= timestamp
            and suite == "trixie-proposed-updates"
            and arch == "binary-mipsel"
        ):
            # mipsel is no longer in the Architectures field of Release
            # but still has zero-sized main/binary-mipsel/Packages
            assert "mipsel" not in release_arches
            return False
    elif archive == "debian-backports":
        if (
            "20110206T182604Z" <= timestamp <= "20110323T163740Z"
            and suite == "squeeze-backports"
            and arch == "binary-hurd-i386"
        ):
            # Packages file is not in the Release file and also doesn't appear
            # in Architecture field
            assert "hurd-i386" not in release_arches
            return False
    elif archive == "debian-ports":
        if (
            "20081002T000000Z" <= timestamp <= "20090507T000000Z"
            and suite == "unreleased"
            and arch == "binary-sh4"
        ):
            # missing Packages file
            assert "sh4" not in release_arches
            return False
        elif (
            "20120612T151839Z" <= timestamp <= "20190421T104122Z"
            and suite in ["experimental", "unstable"]
            and arch == "binary-hurd-i386"
        ):
            # the Packages file is not in the Release file
            return False
        if timestamp == "20121225T133343Z" and arch == "binary-x32":
            # the Packages file is 404
            return False
        if "20141107T013310Z" <= timestamp and arch == "binary-arm64":
            # the Packages file is not in the Release file
            return False
        if (
            "20141209T125301Z" <= timestamp <= "20141209T182928Z"
            and suite == "unreleased"
            and arch in ["binary-ppc64", "binary-sh4", "binary-sparc64", "binary-x32"]
        ):
            # the Packages file is not in the Release file
            return False
        if (
            timestamp == "20141209T182928Z"
            and suite == "unreleased"
            and arch in ["binary-hurd-i386", "binary-m68k", "binary-powerpcspe"]
        ):
            # the Packages file is not in the Release file
            return False
    elif archive == "debian-security":
        if (
            timestamp == "20050803T000000Z"
            and suite == "sarge"
            and arch == "binary-amd64"
        ):
            # missing Packages file
            assert "amd64" not in release_arches
            return False
        elif (
            timestamp == "20051009T000000Z"
            and suite == "potato"
            and arch
            in [
                "binary-alpha",
                "binary-arm",
                "binary-i386",
                "binary-m68k",
                "binary-powerpc",
                "binary-sparc",
            ]
        ):
            # the Packages files are 404
            return False
        elif (
            "20180626T155643Z" <= timestamp <= "20180627T213342Z"
            and suite == "jessie"
            and arch
            in [
                "binary-arm64",
                "binary-mips",
                "binary-mipsel",
                "binary-powerpc",
                "binary-ppc64el",
                "binary-s390x",
            ]
        ):
            # the Packages files are 404
            return False
    return True


def skip_verify12(archive, timestamp, suite, area, arch):
    skip_verify1 = False
    skip_verify2 = False
    if archive == "debian":
        if timestamp == "20060803T000000Z":
            # semi-random mess of Packages files with and without correct
            # checksums across all suites except woody
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20070408T000000Z"
            and suite == "etch-proposed-updates"
            and arch == "amd64"
        ):
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20070723T000000Z"
            and suite == "sid"
            and area == "main"
            and arch == "hppa"
        ):
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20070803T000000Z"
            and suite == "experimental"
            and area == "main"
            and arch == "arm"
        ):
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20080501T000000Z" and suite == "sid" and area == "main":
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20080801T000000Z"
            and suite == "sid"
            and area == "non-free"
            and arch == "amd64"
        ):
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20080816T000000Z" and suite == "lenny" and area == "main":
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20080816T000000Z"
            and suite == "sid"
            and area in ["contrib", "non-free"]
        ):
            skip_verify1 = skip_verify2 = True
        elif "20090122T031917Z" <= timestamp <= "20090122T091438Z":
            # only the compressed checksum differs, the uncompressed data verifies correctly
            skip_verify1 = True
        elif timestamp == "20091101T033049Z" and suite in [
            "sid",
            "experimental",
            "squeeze",
            "squeeze-proposed-updates",
        ]:
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20101003T163248Z" and suite == "sid" and area == "main":
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20101003T163248Z" and suite == "squeeze-proposed-updates":
            skip_verify1 = skip_verify2 = True
        elif (
            "20110312T103459Z" <= timestamp <= "20110312T142115Z"
            and suite == "experimental"
            and area == "main"
        ):
            skip_verify1 = skip_verify2 = True
        elif "20110312T103459Z" <= timestamp <= "20110312T142115Z" and suite == "sid":
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20110312T142115Z"
            and suite == "squeeze-proposed-updates"
            and area == "main"
            and arch == "mips"
        ):
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20110312T142115Z" and suite == "wheezy":
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20131211T011114Z"
            and suite == "experimental"
            and area == "main"
        ):
            skip_verify1 = skip_verify2 = True
        elif (
            "20230610T205311Z" <= timestamp <= "20230611T103552Z"
            and suite == "buster-proposed-updates"
        ):
            skip_verify1 = skip_verify2 = True
    elif archive == "debian-backports":
        if (
            "20090114T125110Z" <= timestamp
            and suite == "sarge-backports"
            and area == "main"
            and arch == "mipsel"
        ):
            # the size is wrong
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20121011T163822Z"
            and suite == "squeeze-backports"
            and area == "main"
            and arch in ["s390", "sparc"]
        ):
            # the size is wrong
            skip_verify1 = skip_verify2 = True
    elif archive == "debian-security":
        if timestamp == "20090122T171607Z":
            # compressed checksums are wrong but uncompressed checksums match
            skip_verify1 = True
        elif timestamp == "20090924T122723Z" and suite == "etch" and area == "main":
            # the size is wrong
            skip_verify1 = skip_verify2 = True
        elif (
            timestamp == "20091109T223701Z"
            and suite == "lenny"
            and area == "main"
            and arch == "arm"
        ):
            skip_verify1 = skip_verify2 = True
        elif (
            "20110206T001511Z" <= timestamp <= "20110324T112420Z"
            and suite == "lenny"
            and arch in ["kfreebsd-amd64", "kfreebsd-i386"]
        ):
            # we manually added the entries for kfreebsd to the Release file
            skip_verify1 = skip_verify2 = True
        elif (
            "20110206T001511Z" <= timestamp <= "20110324T112420Z"
            and suite == "squeeze"
            and arch in ["alpha", "hppa"]
        ):
            # we manually added the entries for alpha and hppa to the Release file
            skip_verify1 = skip_verify2 = True
        elif (
            "20110206T001511Z" <= timestamp <= "20110324T112420Z"
            and suite == "wheezy"
            and arch in ["alpha", "hppa", "arm"]
        ):
            # we manually added the entries for alpha and hppa to the Release file
            skip_verify1 = skip_verify2 = True
        elif timestamp == "20190706T232106Z" and suite == "stretch" and area == "main":
            # wrong hash
            skip_verify1 = skip_verify2 = True
    elif archive == "debian-ports":
        if timestamp == "20110816T200334Z":
            if suite == "experimental" and area == "main" and arch == "sh4":
                skip_verify2 = True
            elif suite == "unreleased" and area == "main" and arch == "s390x":
                skip_verify2 = True
            elif suite == "unstable" and area == "main":
                skip_verify2 = True
        elif (
            timestamp == "20111213T202510Z"
            and suite == "unstable"
            and arch in ["sh4", "sparc64"]
        ):
            # wrong hash
            skip_verify1 = skip_verify2 = True
    return (skip_verify1, skip_verify2)


def fixup_data(archive, timestamp, suite, area, arch, data):
    if archive == "debian-security":
        if (
            timestamp == "20111203T174538Z"
            and suite == "lenny"
            and area == "main"
            and arch
            in [
                "alpha",
                "amd64",
                "arm",
                "hppa",
                "i386",
                "ia64",
                "mips",
                "mipsel",
                "powerpc",
                "s390",
                "sparc",
            ]
        ):
            # some Package stanzas are just "None" in all arches except armel
            return data.replace(b"\nNone\n", b"")
        elif (
            timestamp == "20120227T174607Z"
            and suite == "squeeze"
            and area in ["main", "contrib"]
            and arch == "amd64"
        ):
            return data.replace(b"\nNone\n", b"")
    return data
