#!/usr/bin/env python3

# 	Command being timed: "python3 run2_sqlite.py debian"
# 	User time (seconds): 237527.45
# 	System time (seconds): 8394.10
# 	Percent of CPU this job got: 99%
# 	Elapsed (wall clock) time (h:mm:ss or m:ss): 68:54:43
# 	Maximum resident set size (kbytes): 5725444
# 	Major (requiring I/O) page faults: 131528
# 	Minor (reclaiming a frame) page faults: 7622873430
# 	Voluntary context switches: 175518
# 	Involuntary context switches: 7653204
# 	File system inputs: 18847104
# 	File system outputs: 440896632


import sys
import subprocess
import os
from collections import defaultdict
from datetime import datetime
from pathlib import Path
import sqlite3
import time

if len(sys.argv) not in (2, 3, 6):
    print("usage: %s archive [numts [onlysuite onlycomp onlyarch]]" % sys.argv[0])
    exit(1)
archive = sys.argv[1]
numts = -1
onlysuite = None
onlycomp = None
onlyarch = None
if len(sys.argv) >= 3:
    numts = int(sys.argv[2])
if len(sys.argv) == 6:
    onlysuite, onlycomp, onlyarch = sys.argv[3:6]
if numts == -1:
    numts = None

possible_areas = ["main", "contrib", "non-free", "non-free-firmware"]

timestamps = (
    subprocess.check_output(["git", "-C", "./by-timestamp/%s.git" % archive, "tag"])
    .decode("utf-8")
    .splitlines()
)
subprocess.check_call(
    ["git", "-C", "./by-timestamp/%s.git" % archive, "reset", "--hard", "main"]
)

Path("by-package").mkdir(exist_ok=True)
db_existed = os.path.exists("by-package/%s.sqlite3" % archive)
conn = sqlite3.connect("by-package/%s.sqlite3" % archive)
conn.execute("PRAGMA journal_mode=WAL")

lastversions = dict()
lasttimestamp = None
lastarches = dict()
timestamp2id = dict()
suite2id = dict()
arch2id = dict()
pkg2id = dict()
ver2id = dict()

if db_existed:
    timestamp2id = {
        name: i
        for name, i in conn.execute("select name, id from timestamps").fetchall()
    }
    # make sure that the first X timestamps in the DB are also the first X
    # timestamps in the git tag
    if sorted(timestamp2id.keys()) != timestamps[: len(timestamp2id)]:
        print("in git tags but not in db:")
        print(set(timestamps[: len(timestamp2id)]) - set(timestamp2id.keys()))
        print("in db but not in git tags:")
        print(set(timestamp2id.keys() - set(timestamps[: len(timestamp2id)])))
        exit(1)
    lasttimestamp = timestamps[len(timestamp2id) - 1]
    assert lasttimestamp == sorted(timestamp2id.keys())[-1]
    # skip the timestamps that are already in the db
    timestamps = timestamps[len(timestamp2id) :]
    if len(timestamps) == 0:
        print("nothing to do")
        exit(0)
    suite2id = {
        name: i for name, i in conn.execute("select name, id from suites").fetchall()
    }
    arch2id = {
        name: i for name, i in conn.execute("select name, id from arches").fetchall()
    }
    pkg2id = {
        name: i for name, i in conn.execute("select name, id from pkgs").fetchall()
    }
    ver2id = {
        name: i for name, i in conn.execute("select name, id from vers").fetchall()
    }
    rows = conn.execute(
        """
        select suites.name, ranges.comp, arches.name, pkgs.name, vers.name, begin.name
    from ranges
    inner join suites on suites.id = ranges.suite
    inner join arches on arches.id = ranges.arch
    inner join pkgs on pkgs.id = ranges.pkg
    inner join vers on vers.id = ranges.ver
    inner join timestamps as begin on begin.id = ranges.begin
    inner join timestamps as end on end.id = ranges.end
    where end.name = ?
            """,
        (lasttimestamp,),
    ).fetchall()
    for suite, compid, arch, pkg, ver, begin in rows:
        comp = possible_areas[compid]
        key = suite + " " + comp + " " + arch
        if key not in lastversions:
            lastversions[key] = {pkg: {ver: begin}}
        elif pkg not in lastversions[key]:
            lastversions[key][pkg] = {ver: begin}
        else:
            assert ver not in lastversions[key][pkg]
            lastversions[key][pkg][ver] = begin
        if suite not in lastarches:
            lastarches[suite] = set([arch])
        else:
            lastarches[suite].add(arch)

    # delete all rows ending at the last timestamp -- they will be inserted
    # again later
    conn.execute("""delete from ranges where end = ?""", (timestamp2id[lasttimestamp],))
else:
    # The rowid eats so much space, that a table without rowid (but a primary
    # key over several columns) is *smaller* than a table with rowid (but
    # without index). The difference is even more pronounced once an index is
    # created. For the table without rowid we don't need an index, because the
    # primary key fulfills its function. Some numbers for first 47 Mill rows:
    #
    # no rowid: 1.1G
    # with rowid: 1.3G
    # with rowid and index over pkg/ver/arch: 2.0G
    #
    # According to https://www.sqlite.org/withoutrowid.html tables without
    # rowid work best if each row is less than 1/20th the size of a database
    # page. Since 2016 the default page size is 4096 bytes. For the first 47
    # Mill rows, the average page size (without rowid) is 24 bytes, which is
    # about 1/200.
    #
    # Find the table size: select sum(pgsize) from dbstat where name = 'ranges'
    # Find number of rows: select count(*) from ranges
    #
    # The docs also say "The WITHOUT ROWID optimization is likely to be helpful
    # for tables that have non-integer or composite (multi-column) PRIMARY KEYs
    # and that do not store large strings or BLOBs." -- we meet this criteria.
    #
    # The order of columns in the primary key is important. The created B-Tree
    # will only be used if the WHEN conditions in a SELECT do not skip columns
    # in the primary key and are listed in that order. Thus, the columns that
    # are most often part of the query should go to the left and the least used
    # should go to the right of the column list in the primary key
    # specification.
    #
    # We have to include the begin column, because sometimes there are gaps
    # where a package of a specific version and architecture vanishes from a
    # suite in a timestamp but re-appears at a later timestamp. Thus, only
    # creating a key on pkg,arch,ver,suite,comp would not be unique.
    conn.executescript(
        """
create table suites (id integer primary key, name text unique);
create table arches (id integer primary key, name text unique);
create table pkgs (id integer primary key, name text unique);
create table vers (id integer primary key, name text unique);
create table timestamps (id integer primary key, name text unique);
create table ranges (
    suite integer not null references suites,
    comp integer not null,
    arch integer not null references arches,
    pkg integer not null references pkgs,
    ver integer not null references vers,
    begin integer not null references timestamps,
    end integer not null references timestamps,
    primary key (pkg,arch,ver,suite,comp,begin)
) without rowid;
"""
    )

if numts is not None:
    timestamps = timestamps[:numts]

starttime = time.time()

for i, timestamp in enumerate(timestamps):
    print(timestamp)
    subprocess.check_call(
        [
            "git",
            "-C",
            "./by-timestamp/%s.git" % archive,
            "checkout",
            "--quiet",
            timestamp,
        ]
    )
    thisarches = defaultdict(set)
    thisversions = dict()
    cur = conn.execute("insert into timestamps(name) values(?)", (timestamp,))
    timestamp2id[timestamp] = cur.lastrowid
    for suite in sorted(os.listdir("./by-timestamp/%s.git" % archive)):
        if suite == ".git":
            continue
        if onlysuite is not None and onlysuite != suite:
            continue
        if suite not in suite2id:
            cur = conn.execute("insert into suites(name) values(?)", (suite,))
            suite2id[suite] = cur.lastrowid
        for compid, comp in enumerate(possible_areas):
            if onlycomp is not None and onlycomp != comp:
                continue
            if not os.path.exists(
                "./by-timestamp/%s.git/%s/%s" % (archive, suite, comp)
            ):
                # Sometimes directories are empty (first time for
                # jessie-updates on 20190404T213121Z) and then git will not
                # store them. This also breaks symlinks. We just skip those
                # cases.
                continue
            for arch in sorted(
                os.listdir("./by-timestamp/%s.git/%s/%s" % (archive, suite, comp))
            ):
                if onlyarch is not None and onlyarch != arch:
                    continue
                if arch not in arch2id:
                    cur = conn.execute("insert into arches(name) values(?)", (arch,))
                    arch2id[arch] = cur.lastrowid
                thisarches[suite].add(arch)
                key = suite + " " + comp + " " + arch
                thisversions[key] = dict()
                with open(
                    "./by-timestamp/%s.git/%s/%s/%s" % (archive, suite, comp, arch),
                    encoding="ascii",
                ) as f:
                    for line in f:
                        pkg, ver = line.rstrip().split(" ", maxsplit=1)
                        if pkg not in pkg2id:
                            cur = conn.execute(
                                "insert into pkgs(name) values(?)", (pkg,)
                            )
                            pkg2id[pkg] = cur.lastrowid
                        if ver not in ver2id:
                            cur = conn.execute(
                                "insert into vers(name) values(?)", (ver,)
                            )
                            ver2id[ver] = cur.lastrowid
                        if pkg in thisversions[key]:
                            thisversions[key][pkg].add(ver)
                        else:
                            thisversions[key][pkg] = set([ver])

                for pkg in thisversions[key]:
                    if key in lastversions:
                        last_ver = lastversions[key].get(pkg)
                    else:
                        last_ver = None
                    if timestamp == timestamps[-1]:
                        rows = []
                        # special handling for last timestamp
                        if last_ver is None:
                            for ver in thisversions[key][pkg]:
                                rows.append((ver, timestamp, timestamp))
                        else:
                            if last_ver.keys() == thisversions[key][pkg]:
                                for ver in last_ver:
                                    rows.append((ver, last_ver[ver], timestamp))
                            else:
                                # there is a difference
                                for ver in sorted(
                                    last_ver.keys() | thisversions[key][pkg]
                                ):
                                    if (
                                        ver in last_ver
                                        and ver in thisversions[key][pkg]
                                    ):
                                        # version is in both, the last and current timestamp
                                        rows.append((ver, last_ver[ver], timestamp))
                                    elif (
                                        ver in last_ver
                                        and ver not in thisversions[key][pkg]
                                    ):
                                        # version is in last but not current timestamp
                                        rows.append((ver, last_ver[ver], lasttimestamp))
                                    elif (
                                        ver not in last_ver
                                        and ver in thisversions[key][pkg]
                                    ):
                                        # version is new in this timestamp
                                        rows.append((ver, timestamp, timestamp))
                                    else:
                                        raise Exception("logic error")
                        conn.executemany(
                            """insert into ranges(suite, comp, arch, pkg, ver, begin, end)
                            values(?, ?, ?, ?, ?, ?, ?)""",
                            [
                                (
                                    suite2id[suite],
                                    compid,
                                    arch2id[arch],
                                    pkg2id[pkg],
                                    ver2id[v],
                                    timestamp2id[b],
                                    timestamp2id[e],
                                )
                                for v, b, e in rows
                            ],
                        )
                        if last_ver is not None:
                            del lastversions[key][pkg]
                            if not lastversions[key]:
                                del lastversions[key]
                        continue
                    if last_ver is None:
                        # package didn't exist before, start new entry
                        if key not in lastversions:
                            lastversions[key] = dict()
                        lastversions[key][pkg] = {
                            v: timestamp for v in thisversions[key][pkg]
                        }
                        continue
                    if last_ver.keys() == thisversions[key][pkg]:
                        # same versions in this Packages file as in the last
                        continue
                    # there is a difference -- add a new line to the output for
                    # each version that was removed and add a new entry in the
                    # dict for each version that was added
                    rows = []
                    for ver in last_ver.keys() - thisversions[key][pkg]:
                        # for all removed versions, add a new line
                        rows.append((ver, last_ver[ver], lasttimestamp))
                        # and delete the old entry
                        del lastversions[key][pkg][ver]
                        if not lastversions[key][pkg]:
                            del lastversions[key][pkg]
                    if not lastversions[key]:
                        del lastversions[key]
                    conn.executemany(
                        """insert into ranges(suite, comp, arch, pkg, ver, begin, end)
                        values(?, ?, ?, ?, ?, ?, ?)""",
                        [
                            (
                                suite2id[suite],
                                compid,
                                arch2id[arch],
                                pkg2id[pkg],
                                ver2id[v],
                                timestamp2id[b],
                                timestamp2id[e],
                            )
                            for v, b, e in rows
                        ],
                    )
                    for ver in thisversions[key][pkg] - last_ver.keys():
                        # for all added versions, add a new entry
                        if key not in lastversions:
                            lastversions[key] = dict()
                        if pkg not in lastversions[key]:
                            lastversions[key][pkg] = dict()
                        lastversions[key][pkg][ver] = timestamp

                # go through all packages that were handled last timestamp but
                # were missing this timestamp
                if key in lastversions:
                    for pkg in lastversions[key].keys() - thisversions[key].keys():
                        rows = []
                        for ver in lastversions[key][pkg]:
                            rows.append(
                                (ver, lastversions[key][pkg][ver], lasttimestamp)
                            )
                        conn.executemany(
                            """insert into ranges(suite, comp, arch, pkg, ver, begin, end)
                            values(?, ?, ?, ?, ?, ?, ?)""",
                            [
                                (
                                    suite2id[suite],
                                    compid,
                                    arch2id[arch],
                                    pkg2id[pkg],
                                    ver2id[v],
                                    timestamp2id[b],
                                    timestamp2id[e],
                                )
                                for v, b, e in rows
                            ],
                        )
                        del lastversions[key][pkg]
                    if not lastversions[key]:
                        del lastversions[key]
    # add entries for all suites that were present in the last timestamp
    # but not anymore in this timestamp
    for suite in lastarches.keys() - thisarches.keys():
        for arch in lastarches[suite]:
            for compid, comp in enumerate(possible_areas):
                if onlycomp is not None and onlycomp != comp:
                    continue
                key = suite + " " + comp + " " + arch
                if key not in lastversions:
                    continue
                for pkg in lastversions[key]:
                    rows = []
                    for ver in lastversions[key][pkg]:
                        rows.append((ver, lastversions[key][pkg][ver], lasttimestamp))
                    conn.executemany(
                        """insert into ranges(suite, comp, arch, pkg, ver, begin, end)
                        values(?, ?, ?, ?, ?, ?, ?)""",
                        [
                            (
                                suite2id[suite],
                                compid,
                                arch2id[arch],
                                pkg2id[pkg],
                                ver2id[v],
                                timestamp2id[b],
                                timestamp2id[e],
                            )
                            for v, b, e in rows
                        ],
                    )
                del lastversions[key]
    # handle architectures that were present in the last timestamp but not
    # anymore in this one
    for suite in lastarches.keys() & thisarches.keys():
        for arch in lastarches[suite] - thisarches[suite]:
            for compid, comp in enumerate(possible_areas):
                if onlycomp is not None and onlycomp != comp:
                    continue
                key = suite + " " + comp + " " + arch
                if key not in lastversions:
                    continue
                for pkg in lastversions[key]:
                    rows = []
                    for ver in lastversions[key][pkg]:
                        rows.append((ver, lastversions[key][pkg][ver], lasttimestamp))
                    conn.executemany(
                        """insert into ranges(suite, comp, arch, pkg, ver, begin, end)
                        values(?, ?, ?, ?, ?, ?, ?)""",
                        [
                            (
                                suite2id[suite],
                                compid,
                                arch2id[arch],
                                pkg2id[pkg],
                                ver2id[v],
                                timestamp2id[b],
                                timestamp2id[e],
                            )
                            for v, b, e in rows
                        ],
                    )
                del lastversions[key]
    lastarches = thisarches
    lasttimestamp = timestamp
    total_hours = (time.time() - starttime) / (60 * 60)
    ts_per_h = (i + 1) / total_hours
    remaining = (len(timestamps) - i - 1) / ts_per_h
    print("per hour: %f remaining: %f hours" % (ts_per_h, remaining))

print("committing changes to the database...")

# no need to create an index because we have a primary key over nearly all cols
# conn.execute("create index idx0 on ranges(pkg,arch,ver)")

# From https://www.sqlite.org/faq.html#q19
# "SQLite will easily do 50,000 or more INSERT statements per second on an
# average desktop computer. But it will only do a few dozen transactions
# per second."
#
# We only commit at the end of the script and not after each timestamp. If we
# would commit after each timestamp, then we would not keep track of still
# to-be-written ranges in the lastversions dict but write out ranges after
# each timestamp and update these ranges every timestamp. Doing this is far
# slower than keeping track of the ranges in the lastversions dict and only
# writing a range once it's finished.
conn.commit()
conn.close()

subprocess.check_call(
    ["git", "-C", "./by-timestamp/%s.git" % archive, "checkout", "--quiet", "main"]
)
